/**
 * Created by Carlos on 15/12/2017.
 */

/**
 * Created by Carlos on 15/12/2017.
 *
 * [ajaxSetup description]
 * @return Seguridad para todas las peticiones ajax se pasen con token
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

/**
 * Created by Carlos on 15/12/2017.
 *
 * [alertaSimple description]
 * @textoAlerta Texto que mostrara la alerta
 * @tipoAalerta Tipo de alerta (succes,info,error,warning)
 * @return Retorna una alerta con sweetlalert2
 */
const alertaSimple = (titleAlerta = '', textoAlerta, tipoAalerta = null, tiempoAlerta = 2000, outsideClick = false, escapeKey = false, enterKey = false) => {
    swal({
        title: titleAlerta,
        text: textoAlerta,
        type: tipoAalerta,
        showConfirmButton: false,
        showCancelButton: false,
        allowOutsideClick: outsideClick,
        allowEscapeKey: escapeKey,
        allowEnterKey: enterKey,
        timer: tiempoAlerta
    })
}

/**
 * Created by Carlos on 15/12/2017.
 *
 * [responseModal description]
 * @nameRoute Nombre del cuerpo del modal a donde se cargara la informacion
 * @routeLoad La ruta que se cargara en el modal
 * @valID Se pasa un valor solo si va usarse un UpdateCreate
 * @return Retorna lo que toma de la ruta al modal
 */
const responseModal = (nameRoute, routeLoad, valID = {}, typeRequest = 'POST') => {
    let imageLoading = `<div class="cssload-container"><div class="cssload-lt"></div><div class="cssload-rt"></div><div class="cssload-lb"></div><div class="cssload-rb"></div></div>`
    let token = $('meta[name="csrf-token"]').attr('content')
    $.ajax({
        type : typeRequest,
        url : routeLoad,
        data: {
            _token: token,
            valueID: valID
        },
        beforeSend: function(){
            $(nameRoute).html(imageLoading)
        },
        success:function(data){
            $(nameRoute).html(data)
        }
    })
}

const responseModalChangeUser = (nameRoute, routeLoad, valID = {}, typeRequest = 'POST') => {
    let imageLoading = `<div class="cssload-container"><div class="cssload-lt"></div><div class="cssload-rt"></div><div class="cssload-lb"></div><div class="cssload-rb"></div></div>`
    let token = $('meta[name="csrf-token"]').attr('content')
    let selected = []
    $('input.checkChildren:checkbox').filter(':checked').each(function() {
        selected.push($(this).val());
    })
    $.ajax({
        type : typeRequest,
        url : routeLoad,
        data: {
            _token: token,
            valueID: valID,
            checkInput: selected
        },
        beforeSend: function(){
            $(nameRoute).html(imageLoading)
        },
        success:function(data){
            $(nameRoute).html(data)
        }
    })
}

// Función para cerrar un modal y limpiar el cuerpo del mismo (Funciona con la función responseModal)
const clearModal = (modalID, bodyModal) => {
    $(bodyModal).html('').promise().done(function() {
        $('#'+modalID).modal('toggle')
    })
}

// Función para escuchar el evento cuando se cierra un modal y limpiar el cuerpo del mismo (Funciona con la función responseModal)
const clearModalClose = (modalID, bodyModal) => {
    $('#'+modalID).on('hidden.bs.modal', function () {
        $(bodyModal).html('')
    })
}

// Función que muestra un boton de carga cuando se ejecuta un formulario
const changeButtonForm = (btnShow, btnHide) => {
    $('.'+btnShow).hide()
    $('.'+btnHide).show()
}

// Función que muestra los errores que emite el controlador en un DIV
const showErrorForm = (data, formDiv) => {
    let errors = ''
    for(datos in data.responseJSON){
        errors += '<strong>' + data.responseJSON[datos] + '</strong><br>'
    }
    $(formDiv).fadeIn().html(
        '<span class="fa fa-close"></span> <strong>Error</strong>'+
        '<hr class="message-inner-separator">'+
        '<span>' + errors + '</span>'
    )
}

// Función para ocultar los errores cuando se ingresa en algun campo que se requeria data
const hideErrorForm = (formDiv) => {
    $("input[type=text],input[type=checkbox],input[type=password]").click(function() {
        $(formDiv).fadeOut().html()
    })
}

// Función que devuelve los datos que no se repiten en un array
const arrayUnique = (value, index, self) => {
    return self.indexOf(value) === index
}

// Función que corrigue al grafico de area [ChartJS]
const hex2rgba_convert = (hex,opacity) => {
    hex = hex.replace('#','')
    r = parseInt(hex.substring(0,2), 16)
    g = parseInt(hex.substring(2,4), 16)
    b = parseInt(hex.substring(4,6), 16)

    result = 'rgba('+r+','+g+','+b+','+opacity/100+')'
    return result
}

// Funcion que obtiene la data para los graficos
const axiosChart = (requestType, url, parameters = {}) => {
    alertaSimple('', '<h4><i class="fa fa-spin fa-spinner"></i> Cargando</h4>', null, null, true, true, true)
    axios[requestType](url, { params: parameters })
        .then(response => {
            swal.close()
            $('.chartHTML').html(response.data.chartHTML)
            $('.chartScript').html(response.data.chartScript)
            $('.chartTable').html(response.data.chartTable);
            tableSumChart()
        }).catch(error => console.log(error))
}

// Funcion que suma los campos de un tabla de manera vertical
const tableSumChart = () => {
    $('#sum_table > tbody > tr:first > td').each(function(){
        let $td = $(this)
        let colTotal = 0
        $('#sum_table > tbody > tr:not(:first,.totalColumn)').each(function(){ colTotal  += parseFloat($(this).children().not(':eq(0)').eq($td.index()).html()) })
        $('#sum_table > tbody > tr.totalColumn > td.totalCol').eq($td.index()).html('<strong>'+parseFloat(colTotal).toFixed(2)+'</strong>')
    })
}

// Funcion que activa el select para escoger filtro por mes / año y rango de fechas
const selectMesAnioRange = () => {
    $('#selectFilterDate').change(function() {
        let value = this.value
        if(value === 'mesanio'){
            $('#rango').hide()
            $('#mesanio').show()
        }else{
            $('#mesanio').hide()
            $('#rango').show()
        }
    })
}

// Función que deshabilita y coloca estilo a la fila de la tabla
const markCheckAll = (checkGeneral, checkChildren) => {
    if ($(checkGeneral).is(':checked')) {
        $(`${checkChildren}:checkbox`).prop("checked", "checked");
    } else {
        $(`${checkChildren}:checkbox`).prop("checked", false);
    }
}
