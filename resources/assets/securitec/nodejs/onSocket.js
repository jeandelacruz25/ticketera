socketNodejs.on('reloadTableTareas', function () {
    if(typeof vmTableTareas !== 'undefined') vmTableTareas.nodeFetchTareas()
    console.log('[socketNodejs] Se actualizaron las tareas')
})

socketNodejs.on('reloadComentarios', function () {
    if (typeof vmTicketComentarios !== 'undefined') vmTicketComentarios.fetchComentariosRefresh()
    console.log('[socketNodejs] Se actualizaron los comentarios')
})
