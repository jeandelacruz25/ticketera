const socketNodejs = io.connect(serverNodeTicketera, {
    'reconnection': true,
    'reconnectionAttempts': 15,
    'reconnectionDelay': 9000,
    'reconnectionDelayMax': 9000
})

socketNodejs.on('connect', function () {
    console.log('socketNodejs connected!')
})

socketNodejs.on('connect_error', function () {
    swal.close()
    let timeInterval = 9
    let refreshIntervalId = setInterval(() => {
        alertaSimple('', `Fallo la conexión con el socket del Server NodeJS volveremos a reintentar en ${timeInterval} segundos!!!`, 'error', null, true, true, true)
        timeInterval--
        if (timeInterval === 0) clearInterval(refreshIntervalId)
    }, 1000)
    console.log('socketNodejs Connection Failed')
})

socketNodejs.on('disconnect', function () {
    alertaSimple('', 'Acabas de perder conexión con el socket del Server Nodejs !!!', 'error', null, true, true, true)
    console.log('socketNodejs Disconnected')
})
