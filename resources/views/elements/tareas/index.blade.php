@extends('layouts.layoutApp', ['module' => $titleModule])

@section('content')
    <section class="content-header">
        <h1>
            {{ $titleModule }}
            <small>{{ $titleSubModule }}</small>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    @if(auth()->user()->id_rol == 1)
                        @include('elements.tareas.utils.tables.table_admin')
                    @else
                        @include('elements.tareas.utils.tables.table_user')
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
