<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritecLarge')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalle de Ticket / Tarea</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#ticket" data-toggle="tab">Ticket</a></li>
                        <li><a href="#tarea" data-toggle="tab">Tarea</a></li>
                        <li><a href="#comentarios" data-toggle="tab" onclick="vmTicketComentarios.fetchComentarios('{{ $dataTicket['id'] }}')">Comentarios [Ticket]</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="ticket">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Asunto</label>
                                        <input type="text" class="form-control" value="{{ $dataTicket['asunto'] }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fecha Registro</label>
                                        <input type="text" class="form-control" value="{{ $dataTicket['fecha_reg'] ? \Carbon\Carbon::parse($dataTicket['fecha_reg'])->format('d/m/Y H:i') : '' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Detalle</label>
                                        <div>{!! $dataTicket['detalle'] !!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tarea">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fecha Inicio</label>
                                        <input type="text" class="form-control" value="{{ $dataTarea['fecha_inicio'] ? \Carbon\Carbon::parse($dataTarea['fecha_inicio'])->format('d/m/Y H:i') : '' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fecha Vencimiento</label>
                                        <input type="text" class="form-control" value="{{ $dataTarea['fecha_venc'] ? \Carbon\Carbon::parse($dataTarea['fecha_venc'])->format('d/m/Y H:i') : '' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fecha Cierre</label>
                                        <input type="text" class="form-control" value="{{ $dataTarea['fecha_cierre'] ? \Carbon\Carbon::parse($dataTarea['fecha_cierre'])->format('d/m/Y H:i') : '' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Descripción Tarea</label>
                                        <div>{!! $dataTarea['descripcion'] !!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="comentarios">
                            <div class="box box-primary direct-chat direct-chat-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Comentarios del Ticket</h3>
                                </div>
                                <!-- /.box-header -->
                                <div id="ticketComentariosVue">
                                    <div class="box-body">
                                        <template v-if="initializeComentarios">
                                            <div class="direct-chat-messages">
                                                <div class="col-md-12">
                                                    <div class="cssload-container">
                                                        <div class="cssload-lt"></div>
                                                        <div class="cssload-rt"></div>
                                                        <div class="cssload-lb"></div>
                                                        <div class="cssload-rb"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </template>
                                        <template v-else>
                                            <template v-if="dataComentarios.length > 0">
                                                <div class="direct-chat-messages" style="height: 380px !important;">
                                                    <template v-for="(dataComentario, index) in dataComentarios">
                                                        <div class="direct-chat-msg" :class="[dataComentario.usuario.id == idUsuario ? '' : 'right']">
                                                            <div class="direct-chat-info clearfix">
                                                                <span class="direct-chat-name" :class="[dataComentario.usuario.id == idUsuario ? 'pull-left' : 'pull-right']" v-html="dataComentario.usuario.name"></span>
                                                                <span class="direct-chat-timestamp" :class="[dataComentario.usuario.id == idUsuario ? 'pull-right' : 'pull-left']" v-html="fechaReg[index]"></span>
                                                            </div>
                                                            <img class="direct-chat-img" :src="dataAvatars[index].encoded">
                                                            <div class="direct-chat-text" v-html="dataComentario.comentario"></div>
                                                        </div>
                                                    </template>
                                                </div>
                                            </template>
                                            <template v-else>
                                                <div class="direct-chat-messages">
                                                    <div class="col-md-12">
                                                        <div class="alert alert-warning">
                                                            <h4><i class="icon fa fa-warning"></i> No hay ningún comentario agregado en este ticket. </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </template>
                                        </template>
                                    </div>
                                    <div class="box-footer">
                                        <div class="input-group">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <textarea name="commentTicket" class="form-control commentUpdate" rows="3" v-model="textoComentario"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <span class="input-group-btn">
                                                    <template v-if="textoComentario.length > 0 && !loadSubmit">
                                                        <button type="button" class="btn btn-primary btn-flat" @click="guardarComentario">Comentar</button>
                                                    </template>
                                                    <template v-else-if="loadSubmit">
                                                        <button type="button" class="btn btn-primary btn-flat disabled" disabled><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                                                    </template>
                                                    <template v-else-if="textoComentario.length <= 0 && !loadSubmit">
                                                        <button type="button" class="btn btn-primary btn-flat disabled" disabled>Comentar</button>
                                                    </template>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritecLarge')" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/ticketComentariosVue.js?version='.time()) !!}"></script>
<script>
    clearModalClose('modalSecuritec', 'div.dialogSecuritecLarge')

    function customURLConverter(url, node, on_save, name) {
        url = url.replace('../../../', '/')
        return url
    }

    tinyEditor('textarea.commentUpdate', {
        urlconverter_callback : 'customURLConverter',
        language: 'es',
        extended_valid_elements: "table[class=table table-bordered],ul[style=list-style-type: circle;],img[class=img-responsive center-block|!src|border:0|alt|title|width|height|style]",
        theme: "modern",
        height: 300,
        font_formats: "Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n",
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager smileys"
        ],
        auto_convert_smileys: true,
        paste_data_images: true,
        toolbar1: "responsivefilemanager undo redo | formatselect styleselect fontsizeselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink anchor | image table | print media smileys | fullscreen preview",
        style_formats: [
            {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
            {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
            {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
            {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
            {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
            {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
            {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
            {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
            {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
            {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
            {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
            {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
            {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
        ],
        menubar: true,
        fullscreen_new_window : true,
        toolbar_items_size: 'small',
        image_advtab: true,
        fix_list_elements : true,
        external_filemanager_path: "{{ '' }}../js/tinymce/filemanager/",
        filemanager_title: "Gestor de Archivos" ,
        external_plugins: { filemanager: "filemanager/plugin.min.js"},
        branding: false,
        init_instance_callback: function (editor) {
            editor.on('KeyUp', function (e) {
                let textoContenido = e.currentTarget.innerText
                let contenido = e.currentTarget.innerHTML

                if(textoContenido.length > 1){
                    vmTicketComentarios.textoComentario = contenido
                }else{
                    vmTicketComentarios.textoComentario = ''
                }
            })
        }
    })

    vmTicketComentarios.idUsuario = '{{ auth()->id()  }}'
</script>
