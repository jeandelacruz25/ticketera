<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritecLarge')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ $updateForm ? "Editar" : "Agregar" }} Tarea</h4>
    </div>
    <div class="modal-body">
        <div class="loadingFormTareas">
            <div class="row">
                <div class="col-md-12">
                    <div class="cssload-container">
                        <div class="cssload-lt"></div>
                        <div class="cssload-rt"></div>
                        <div class="cssload-lb"></div>
                        <div class="cssload-rb"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="startFormTareas hidden">
            <form id="formTareas">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombre Tarea</label>
                            <input type="text" class="form-control" name="nombreTarea" value="{{ $updateForm ? $dataTarea['tarea'] : '' }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Usuario Asignado</label>
                            <select class="form-control selectBoostrap" name="selectUsuarios" data-live-search="true">
                                @foreach($dataUsuarios as $key => $value)
                                    <option value="{{ $value['id'] }}" @if($updateForm) {{ $dataTarea['id_usuario'] == $value['id'] ? 'selected' : '' }} @else {{ $value['id'] == 11 ? 'selected' : '' }} @endif>{{ $value['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Descripción</label>
                            <textarea name="descripcionTarea" class="form-control tareaEditor" rows="3">{{ $updateForm ? $dataTarea['descripcion'] : '' }}</textarea>
                        </div>
                    </div>
                    @if($updateForm)
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Fecha Registro [Ticket]</label>
                                <input type="text" class="form-control" value="{{ $dataTarea['ticket']['fecha_reg'] ? \Carbon\Carbon::parse($dataTarea['ticket']['fecha_reg'])->format('Y-m-d h:i a') : ''  }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Fecha Inicio</label>
                                <div class="input-group flatpickr datePickerTarea" data-id="strap">
                                    <input type="text" class="form-control flatpickr-input" name="fechaInicio" data-input="" value="{{ $updateForm ? $dataTarea['fecha_inicio'] ? \Carbon\Carbon::parse($dataTarea['fecha_inicio'])->format('Y-m-d') : '' : '' }}" readonly>
                                    <a class="input-group-addon input-button" title="Abrir calendario" data-toggle="" style="cursor: pointer"><i class="fa fa-calendar"></i></a>
                                    <a class="input-group-addon input-button" title="Limpiar fecha" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Hora Inicio</label>
                                <div class="input-group flatpickr timePickerTarea" data-id="strap">
                                    <input type="text" class="form-control flatpickr-input" name="horaInicio" data-input="" value="{{ $updateForm ? $dataTarea['fecha_inicio'] ? \Carbon\Carbon::parse($dataTarea['fecha_inicio'])->format('H:i') : '' : '' }}" readonly>
                                    <a class="input-group-addon input-button" title="Abrir Reloj" data-toggle="" style="cursor: pointer"><i class="fa fa-clock-o"></i></a>
                                    <a class="input-group-addon input-button" title="Limpiar reloj" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Fecha Cierre</label>
                                <div class="input-group flatpickr datePickerTarea" data-id="strap">
                                    <input type="text" class="form-control flatpickr-input" name="fechaCierre" data-input="" value="{{ $updateForm ? $dataTarea['fecha_cierre'] ? \Carbon\Carbon::parse($dataTarea['fecha_cierre'])->format('Y-m-d') : '' : '' }}" readonly>
                                    <a class="input-group-addon input-button" title="Abrir calendario" data-toggle="" style="cursor: pointer"><i class="fa fa-calendar"></i></a>
                                    <a class="input-group-addon input-button" title="Limpiar fecha" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hora Cierre</label>
                                <div class="input-group flatpickr timePickerTarea" data-id="strap">
                                    <input type="text" class="form-control flatpickr-input" name="horaCierre" data-input="" value="{{ $updateForm ? $dataTarea['fecha_cierre'] ? \Carbon\Carbon::parse($dataTarea['fecha_cierre'])->format('H:i') : '' : '' }}" readonly>
                                    <a class="input-group-addon input-button" title="Abrir Reloj" data-toggle="" style="cursor: pointer"><i class="fa fa-clock-o"></i></a>
                                    <a class="input-group-addon input-button" title="Limpiar reloj" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Fecha Vencimiento</label>
                                <div class="input-group flatpickr datePickerTarea" data-id="strap">
                                    <input type="text" class="form-control flatpickr-input" name="fechaVenc" data-input="" value="{{ $updateForm ? $dataTarea['fecha_venc'] ? \Carbon\Carbon::parse($dataTarea['fecha_venc'])->format('Y-m-d') : '' : '' }}" readonly>
                                    <a class="input-group-addon input-button" title="Abrir calendario" data-toggle="" style="cursor: pointer"><i class="fa fa-calendar"></i></a>
                                    <a class="input-group-addon input-button" title="Limpiar fecha" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hora Vencimiento</label>
                                <div class="input-group flatpickr timePickerTarea" data-id="strap">
                                    <input type="text" class="form-control flatpickr-input" name="horaVenc" data-input="" value="{{ $updateForm ? $dataTarea['fecha_venc'] ? \Carbon\Carbon::parse($dataTarea['fecha_venc'])->format('H:i') : '' : '' }}" readonly>
                                    <a class="input-group-addon input-button" title="Abrir Reloj" data-toggle="" style="cursor: pointer"><i class="fa fa-clock-o"></i></a>
                                    <a class="input-group-addon input-button" title="Limpiar reloj" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Dificultad</label>
                                <select name="dificultadTarea" class="form-control selectBoostrap">
                                    @if(!$updateForm) <option value="" selected disabled>-</option> @endif
                                    @foreach($dataDificultad as $key => $value)
                                        <option value="{{ $value['id'] }}" @if($updateForm) {{ $value['id'] == $dataTarea['id_tipodificultad'] ? 'selected' : '' }} @endif>{{ $value['tipo_dificultad'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Estado Dev</label>
                                <select class="form-control selectBoostrap" name="estadoDev">
                                    @if($updateForm)
                                        @foreach($dataEstadoDev as $key => $value)
                                            <option value="{{ $value['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $value['color'] }}; color: #ffffff'>{{ $value['estado'] }}</button>" {{ $dataTarea['id_estadodev'] == $value['id'] ? 'selected' : '' }}>{{ $value['estado'] }}</option>
                                        @endforeach
                                    @else
                                        <option value="1" data-content="<button class='btn btn-xs btn-block' style='background: #02B99B; color: #ffffff'>TO DO</button>">TO DO</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Estado</label>
                                <select class="form-control selectBoostrap" name="estadoTicket">
                                    @if($updateForm)
                                        @foreach($dataEstado as $key => $value)
                                            <option value="{{ $value['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $value['color'] }}; color: #ffffff'>{{ $value['estado'] }}</button>" {{ $dataTarea['id_estadotarea'] == $value['id'] ? 'selected' : '' }}>{{ $value['estado'] }}</option>
                                        @endforeach
                                    @else
                                        <option value="1" data-content="<button class='btn btn-xs btn-block' style='background: #28DE75; color: #ffffff'>ACTIVO</button>">ACTIVO</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    @else
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Fecha Registro [Ticket]</label>
                                <input type="text" class="form-control" value="{{ $dataTicket['fecha_reg'] ? \Carbon\Carbon::parse($dataTicket['fecha_reg'])->format('Y-m-d h:i a') : ''  }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Fecha Vencimiento</label>
                                <div class="input-group flatpickr datePickerTarea" data-id="strap">
                                    <input type="text" class="form-control flatpickr-input" name="fechaVenc" data-input="" value="" readonly>
                                    <a class="input-group-addon input-button" title="Abrir calendario" data-toggle="" style="cursor: pointer"><i class="fa fa-calendar"></i></a>
                                    <a class="input-group-addon input-button" title="Limpiar fecha" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hora Vencimiento</label>
                                <div class="input-group flatpickr timePickerTarea" data-id="strap">
                                    <input type="text" class="form-control flatpickr-input" name="horaVenc" data-input="" value="" readonly>
                                    <a class="input-group-addon input-button" title="Abrir Reloj" data-toggle="" style="cursor: pointer"><i class="fa fa-clock-o"></i></a>
                                    <a class="input-group-addon input-button" title="Limpiar hora" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Dificultad</label>
                                <select name="dificultadTarea" class="form-control selectBoostrap">
                                    <option value="" selected disabled>-</option>
                                    @foreach($dataDificultad as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['tipo_dificultad'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Estado Dev</label>
                                <select class="form-control selectBoostrap" name="estadoDev">
                                    @if($updateForm)
                                        @foreach($dataEstadoDev as $key => $value)
                                            <option value="{{ $value['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $value['color'] }}; color: #ffffff'>{{ $value['estado'] }}</button>" {{ $dataTarea['id_estadodev'] == $value['id'] ? 'selected' : '' }}>{{ $value['estado'] }}</option>
                                        @endforeach
                                    @else
                                        <option value="1" data-content="<button class='btn btn-xs btn-block' style='background: #02B99B; color: #ffffff'>TO DO</button>">TO DO</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Estado</label>
                                <select class="form-control selectBoostrap" name="estadoTicket">
                                    @if($updateForm)
                                        @foreach($dataEstado as $key => $value)
                                            <option value="{{ $value['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $value['color'] }}; color: #ffffff'>{{ $value['estado'] }}</button>" {{ $dataTarea['id_estadotarea'] == $value['id'] ? 'selected' : '' }}>{{ $value['estado'] }}</option>
                                        @endforeach
                                    @else
                                        <option value="1" data-content="<button class='btn btn-xs btn-block' style='background: #28DE75; color: #ffffff'>ACTIVO</button>">ACTIVO</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="alert alert-danger formError" style="display: none"></div>
                <input type="hidden" name="ticketID" value="{{ $idTicket }}">
                <input type="hidden" name="taskID" value="{{ $updateForm ? $dataTarea['id'] : '' }}">
                <input type="hidden" name="taskTicket" value="{{ $ticketTask }}">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btnForm">{!! $updateForm ? "<i class='fa fa-edit'></i> Editar" : "<i class='fa fa-plus'></i> Agregar" !!}</button>
                    <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                    <button type="button" class="btn btn-default" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formTareas.js?version='.date('YmdHis')) !!}"></script>
<script>
    selectPicker('.selectBoostrap')
    dateTimePicker('.datePickerTarea', {
        enableTime: false,
        dateFormat: "Y-m-d",
        wrap: true,
        locale: "es",
    })
    dateTimePicker('.timePickerTarea', {
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        wrap: true,
        locale: "es",
        time_24hr: true
    })

    function customURLConverter(url, node, on_save, name) {
        url = url.replace('../../../', '/')
        return url
    }

    tinyEditor('textarea.tareaEditor', {
        urlconverter_callback : 'customURLConverter',
        language: 'es',
        extended_valid_elements: "table[class=table table-bordered],ul[style=list-style-type: circle;],img[class=img-responsive center-block|!src|border:0|alt|title|width|height|style]",
        theme: "modern",
        height: 300,
        font_formats: "Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n",
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager smileys"
        ],
        auto_convert_smileys: true,
        paste_data_images: true,
        toolbar1: "responsivefilemanager undo redo | formatselect styleselect fontsizeselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink anchor | image table | print media smileys | fullscreen preview",
        style_formats: [
            {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
            {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
            {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
            {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
            {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
            {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
            {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
            {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
            {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
            {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
            {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
            {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
            {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
        ],
        menubar: true,
        fullscreen_new_window : true,
        toolbar_items_size: 'small',
        image_advtab: true,
        fix_list_elements : true,
        external_filemanager_path: "{{ '' }}../js/tinymce/filemanager/",
        filemanager_title: "Gestor de Archivos" ,
        external_plugins: { filemanager: "filemanager/plugin.min.js"},
        branding: false,
        setup: function (editor) {
            editor.on('init', function (e) {
                $('.loadingFormTareas').addClass('hidden')
                $('.startFormTareas').removeClass('hidden')
            })
        }
    })

    hideErrorForm('.formError')
    clearModalClose('modalSecuritec', 'div.dialogSecuritecLarge')
</script>
