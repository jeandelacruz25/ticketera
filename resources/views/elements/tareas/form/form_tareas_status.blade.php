<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar status de Tarea</h4>
    </div>
    <div class="modal-body">
        <form id="formChangeStatus">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>¿ Deseas cambiar el status de esta tarea ?</label>
                    </div>
                </div>
            </div>
            <div class="alert alert-danger formError" style="display: none"></div>
            <input type="hidden" name="taskID" value="{{ $idTask }}">
            <input type="hidden" name="statusID" value="{{ $idStatus }}">
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btnForm"><i class='fa fa-check'></i> Si</button>
                <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                <button type="button" class="btn btn-danger" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal"><i class="fa fa-close"></i> No</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formTareas.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalSecuritec', 'div.dialogSecuritec')
</script>
