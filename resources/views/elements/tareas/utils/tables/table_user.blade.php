<div class="box-header with-border">
    <h3 class="box-title">{{ $titleBox }}</h3>
</div>
<div id="tableTareasUsuarioVue">
    <div class="box-body">
        <div v-if="initializeTareas">
            <div class="col-md-12">
                <div class="cssload-container">
                    <div class="cssload-lt"></div>
                    <div class="cssload-rt"></div>
                    <div class="cssload-lb"></div>
                    <div class="cssload-rb"></div>
                </div>
            </div>
        </div>
        <template v-else-if="!initializeTareas && dataTareas.length === 0 && !initDataPagination">
            <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                <div class="col-md-12">
                    <div class="box-tools">
                        <div class="col-md-3">
                            <select class="form-control selectBoostrap" data-live-search="true" v-model="selectCliente" @change="fetchTareas()">
                                <option value="" selected>Seleccione el cliente</option>
                                @foreach($dataCliente as $key => $valueCliente)
                                    <option value="{{ $valueCliente['id'] }}">{{ $valueCliente['cliente'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control selectBoostrap" v-model="selectEstado" @change="paginationFetchTareas()" data-selected-text-format="count > 0" multiple>
                                @foreach($dataEstado as $key => $valueEstado)
                                    <option value="{{ $valueEstado['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $valueEstado['color'] }}; color: #ffffff'>{{ $valueEstado['estado'] }}</button>">{{ $valueEstado['estado'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm" >
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar Tarea" v-model="searchTarea" @keyup.enter="fetchTareas()">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary" @click="fetchTareas()"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Tipo</th>
                                <th>Tarea</th>
                                <th>Fec.Ini</th>
                                <th>Fec.Cie</th>
                                <th>Fec.Ven</th>
                                <th>Status</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td colspan="9">No hay ningúna tarea registrada actualmente.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                <div class="col-md-12">
                    <div class="box-tools">
                        <div class="col-md-3">
                            <select class="form-control selectBoostrap" data-live-search="true" v-model="selectCliente" @change="fetchTareas()">
                                <option value="" selected>Seleccione el cliente</option>
                                @foreach($dataCliente as $key => $valueCliente)
                                    <option value="{{ $valueCliente['id'] }}">{{ $valueCliente['cliente'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control selectBoostrap" v-model="selectEstado" @change="paginationFetchTareas()" data-selected-text-format="count > 0" multiple>
                                @foreach($dataEstado as $key => $valueEstado)
                                    <option value="{{ $valueEstado['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $valueEstado['color'] }}; color: #ffffff'>{{ $valueEstado['estado'] }}</button>">{{ $valueEstado['estado'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm" >
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar Tarea" v-model="searchTarea" @keyup.enter="fetchTareas()">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary" @click="fetchTareas()"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th>Nro</th>
                                <th>Cliente</th>
                                <th>Tipo</th>
                                <th>Tarea</th>
                                <th>Fec.Ini</th>
                                <th>Fec.Cie</th>
                                <th>Fec.Ven</th>
                                <th>Status</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataTarea, index) in dataTareas">
                                <tr>
                                    <td v-html="pagination.from + index"></td>
                                    <td v-html="nameCliente[index]"></td>
                                    <td v-html="parseInt(dataTarea.ticket.id_tipoticket) != 2 ? 'I' : 'R'"></td>
                                    <td>
                                        <a data-tooltip="tooltip" title="" v-bind:data-original-title="dataTarea.tarea" data-placement="right" style="cursor: pointer" @click="clickEvent('div.dialogSecuritecLarge','/formViewTicket', { idTicket: dataTarea.ticket.id, idTarea: dataTarea.id }, 'POST')" data-toggle="modal" data-target="#modalSecuritec" v-html="nameTarea[index]"></a>
                                    </td>
                                    <td v-html="fechaInicio[index]"></td>
                                    <td v-html="fechaCierre[index]"></td>
                                    <td v-html="fechaVenc[index]"></td>
                                    <td>
                                        <div class="btn-group btn-group-xs">
                                            <button type="button" class="btn dropdown-toggle" v-bind:style="{ 'background-color': dataTarea.env.color, 'color': 'white' }" v-html="dataTarea.env.estado" data-toggle="dropdown" aria-expanded="true"></button>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                <li>
                                                    <template v-if="dataTarea.env.id == 1">
                                                        <button type="button" class="btn btn-xs btn-block" style="background-color: #34909E;color: white;" @click="clickEvent('div.dialogSecuritec','formChangeStatusTask', { idTask: dataTarea.id, idStatus: '1' }, 'POST')" data-toggle="modal" data-target="#modalSecuritec">DOING</button>
                                                    </template>
                                                    <template v-else>
                                                        <button type="button" class="btn btn-xs btn-block" style="background-color: #783EE4;color: white;" @click="clickEvent('div.dialogSecuritec','formChangeStatusTask', { idTask: dataTarea.id, idStatus: '2' }, 'POST')" data-toggle="modal" data-target="#modalSecuritec">DONE</button>
                                                    </template>
                                                </li>
                                            </ul>
                                        </div>

                                    </td>
                                    <td class="text-center" v-bind:style="{'background-color': dataTarea.estado.color, 'color': 'white'}" v-html="dataTarea.estado.estado"></td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
    </div>
    <div class="box-footer clearfix">
        <div class="no-margin pull-right">
            <div v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                <div v-bind:class="!initializeTareas && dataTareas.length > 0 || !initializeTareas ? '' : 'disabledDiv'">
                    <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchTareas()"></pagination>
                </div>
            </div>
        </div>
    </div>
</div>
@section('script')
    <script src="{!! asset('js/vue/tableTareasUsuarioVue.js?version='.time()) !!}"></script>
    <script>
        $(document).ready(function() {
            vmTableTareasUsuario.fetchTareas()
        })
    </script>
@endsection
