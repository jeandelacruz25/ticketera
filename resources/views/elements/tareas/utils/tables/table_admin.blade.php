<div class="box-header with-border">
    <h3 class="box-title">{{ $titleBox }}</h3>
</div>
<div id="tableTareasVue">
    <div class="box-body">
        <div v-if="initializeTareas">
            <div class="col-md-12">
                <div class="cssload-container">
                    <div class="cssload-lt"></div>
                    <div class="cssload-rt"></div>
                    <div class="cssload-lb"></div>
                    <div class="cssload-rb"></div>
                </div>
            </div>
        </div>
        <template v-else-if="!initializeTareas && dataTareas.length === 0 && !initDataPagination">
            <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                <div class="col-md-12">
                    <div class="box-tools">
                        <div class="col-md-3">
                            <select class="form-control selectBoostrap" data-live-search="true" v-model="selectCliente" @change="fetchTareas()">
                                <option value="" selected>Seleccione el cliente</option>
                                @foreach($dataCliente as $key => $valueCliente)
                                    <option value="{{ $valueCliente['id'] }}">{{ $valueCliente['cliente'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control selectBoostrap" data-live-search="true" v-model="selectUsuario" @change="fetchTareas()" >
                                <option value="" selected>Seleccione el usuario</option>
                                @foreach($dataUsuario as $key => $valueTipo)
                                    <option value="{{ $valueTipo['id'] }}">{{ $valueTipo['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control selectBoostrap" v-model="selectEstado" @change="paginationFetchTareas()" data-selected-text-format="count > 0" multiple>
                                @foreach($dataEstado as $key => $valueEstado)
                                    <option value="{{ $valueEstado['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $valueEstado['color'] }}; color: #ffffff'>{{ $valueEstado['estado'] }}</button>">{{ $valueEstado['estado'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm" >
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar Tarea" v-model="searchTarea" @keyup.enter="fetchTareas()">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary" @click="fetchTareas()"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Tipo</th>
                                <th>Tarea</th>
                                <th>Usuario</th>
                                <th>Fec.Ven</th>
                                <th>Estado Dev</th>
                                <th>Estado</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td colspan="9">No hay ningúna tarea registrada actualmente.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                <div class="col-md-12">
                    <div class="box-tools">
                        <div class="col-md-2">
                            <select class="form-control selectBoostrap" data-live-search="true" v-model="selectCliente" data-size="8" @change="fetchTareas()">
                                <option value="" selected>Selec. el cliente</option>
                                @foreach($dataCliente as $key => $valueCliente)
                                    <option value="{{ $valueCliente['id'] }}">{{ $valueCliente['cliente'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control selectBoostrap" data-live-search="true" v-model="selectUsuario" data-size="8" @change="fetchTareas()" >
                                <option value="" selected>Selec. el usuario</option>
                                @foreach($dataUsuario as $key => $valueTipo)
                                    <option value="{{ $valueTipo['id'] }}">{{ $valueTipo['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control selectBoostrap" v-model="selectEstado" @change="paginationFetchTareas()" data-size="8" data-selected-text-format="count > 0" multiple>
                                @foreach($dataEstado as $key => $valueEstado)
                                    <option value="{{ $valueEstado['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $valueEstado['color'] }}; color: #ffffff'>{{ $valueEstado['estado'] }}</button>">{{ $valueEstado['estado'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control selectBoostrap" v-model="selectTipo" data-size="8" @change="fetchTareas()">
                                <option value="" selected>Selec. el tipo</option>
                                @foreach($dataTipo as $key => $valueTipo)
                                    <option value="{{ $valueTipo['id'] }}">{{ $valueTipo['tipoticket'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm" >
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar Tarea" v-model="searchTarea" @keyup.enter="fetchTareas()">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary" @click="fetchTareas()"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12">
                    <div class="pull-right">
                        <button class="btn btn-success btn-sm" @click="clickDownload()"><i class="fa fa-file-excel-o"></i> Descargar Reporte</button>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th>Nro</th>
                                <th>Cliente</th>
                                <th>Tipo</th>
                                <th>Tarea</th>
                                <th>Usuario</th>
                                <th>Fec.Ven</th>
                                <th>Es.Dev</th>
                                <th>Es.</th>
                                <th>Peso</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataTarea, index) in dataTareas">
                                <tr>
                                    <td v-html="pagination.from + index"></td>
                                    <td v-html="nameCliente[index]"></td>
                                    <td v-html="parseInt(dataTarea.ticket.id_tipoticket) != 2 ? 'I' : 'R'"></td>
                                    <td>
                                        <a data-tooltip="tooltip" title="" v-bind:data-original-title="dataTarea.tarea" data-placement="right" style="cursor: pointer" @click="clickEvent('div.dialogSecuritecLarge','/formTareas', { idTicket: dataTarea.ticket.id, idTarea: dataTarea.id, ticketTask: false }, 'POST')" data-toggle="modal" data-target="#modalSecuritec" v-html="nameTarea[index]"></a>
                                    </td>
                                    <td v-html="dataTarea.usuario.name"></td>
                                    <td v-html="fechaVenc[index]"></td>
                                    <td>
                                        <button class="btn btn-xs" v-bind:style="{ 'background-color': dataTarea.env.color, 'color': 'white' }" v-html="dataTarea.env.estado"></button>
                                    </td>
                                    <td class="text-center" v-bind:style="{'background-color': dataTarea.estado.color, 'color': 'white'}" v-html="dataTarea.estado.estado"></td>
                                    <td v-html="dataTarea.orden"></td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-xs" @click="clickEvent('div.dialogSecuritecLarge','formViewTicket', { idTarea: dataTarea.id, idTicket: dataTarea.ticket.id }, 'POST')" data-toggle="modal" data-target="#modalSecuritec"><i class="fa fa-eye"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
    </div>
    <div class="box-footer clearfix">
        <div class="no-margin pull-right">
            <div v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                <div v-bind:class="!initializeTareas && dataTareas.length > 0 || !initializeTareas ? '' : 'disabledDiv'">
                    <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchTareas()"></pagination>
                </div>
            </div>
        </div>
    </div>
</div>
@section('script')
    <script src="{!! asset('js/vue/tableTareasVue.js?version='.time()) !!}"></script>
    <script>
        $(document).ready(function() {
            vmTableTareas.fetchTareas()
        })
    </script>
@endsection
