<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ $updateForm ? "Editar" : "Agregar" }} Cliente</h4>
    </div>
    <div class="modal-body">
        <form id="formCliente">
            <div class="form-group">
                <label>Nombre Cliente</label>
                <input type="text" name="nombreCliente" class="form-control" placeholder="Ingresar el nombre del cliente" value="{{ $updateForm ? $dataCliente['cliente'] : '' }}">
            </div>
            <div class="form-group">
                <label>Persona Contacto</label>
                <input type="text" name="personaContacto" class="form-control" placeholder="Ingrese la persona a contactar" value="{{ $updateForm ? $dataCliente['persona_contacto'] : '' }}">
            </div>
            <div class="form-group">
                <label>Telefono Contacto</label>
                <input type="text" name="telefonoContacto" class="form-control" placeholder="Ingresar el telefono a contactar" value="{{ $updateForm ? $dataCliente['telefono_contacto'] : '' }}">
            </div>
            <div class="form-group">
                <label>Email Contacto</label>
                <input type="text" name="emailContacto" class="form-control" placeholder="Ingresar el correo a contactar" value="{{ $updateForm ? $dataCliente['email_contacto'] : '' }}">
            </div>
            <div class="form-group">
                <label>Observación</label>
                <input type="text" name="observacion" class="form-control" placeholder="Ingresar una observación" value="{{ $updateForm ? $dataCliente['observacion'] : '' }}">
            </div>
            <div class="form-group">
                <label>Peso</label>
                <input type="text" name="pesoCliente" class="form-control" placeholder="Ingresar un peso para el cliente" value="{{ $updateForm ? $dataCliente['peso'] : '' }}">
            </div>
            <div class="alert alert-danger formError" style="display: none"></div>
            <input type="hidden" name="clienteID" value="{{ $updateForm ? $dataCliente['id'] : '' }}">
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btnForm">{!! $updateForm ? "<i class='fa fa-edit'></i> Editar" : "<i class='fa fa-plus'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                <button type="button" class="btn btn-default" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formClientes.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalSecuritec', 'div.dialogSecuritec')
</script>
