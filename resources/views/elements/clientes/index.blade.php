@extends('layouts.layoutApp', ['module' => $titleModule])

@section('content')
    <section class="content-header">
        <h1>
            {{ $titleModule }}
            <small>{{ $titleSubModule }}</small>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div id="tableClientesVue">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $titleBox }}</h3>
                            <div class="box-tools pull-right">
                                <a onclick="responseModal('div.dialogSecuritec','formClientes', {}, 'GET')" data-toggle="modal" data-target="#modalSecuritec" class="btn btn-primary btn-xs">
                                    <i class="fa fa-group" aria-hidden="true"></i> Agregar Cliente
                                </a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div v-if="initializeClientes">
                                <div class="col-md-12">
                                    <div class="cssload-container">
                                        <div class="cssload-lt"></div>
                                        <div class="cssload-rt"></div>
                                        <div class="cssload-lb"></div>
                                        <div class="cssload-rb"></div>
                                    </div>
                                </div>
                            </div>
                            <template v-else-if="!initializeClientes && dataClientes.length === 0 && !initDataPagination">
                                <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                                    <div class="col-md-12">
                                        <div class="box-tools pull-right">
                                            <div class="input-group input-group-sm" style="width: 200px;">
                                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar Cliente" v-model="searchCliente" @keyup.enter="fetchClientes()">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-primary" @click="fetchClientes()"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nombre</th>
                                                <th>Telefono</th>
                                                <th>Peso</th>
                                                <th>Acción</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="text-center">
                                                <td colspan="5">No hay ningún cliente registrado actualmente.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </template>
                            <template v-else>
                                <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                                    <div class="col-md-12">
                                        <div class="box-tools pull-right">
                                            <div class="input-group input-group-sm" style="width: 200px;">
                                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar Cliente" v-model="searchCliente" @keyup.enter="fetchClientes()">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-primary" @click="fetchClientes()"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nombre</th>
                                                    <th>Telefono</th>
                                                    <th>Peso</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <template v-for="(dataCliente, index) in dataClientes">
                                                    <tr>
                                                        <td v-html="pagination.from + index"></td>
                                                        <td v-html="dataCliente.cliente"></td>
                                                        <td v-html="dataCliente.telefono_contacto ? dataCliente.telefono_contacto : '-'"></td>
                                                        <td v-html="dataCliente.peso"></td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-warning btn-xs" @click="clickEvent('div.dialogSecuritec','formClientes', dataCliente.id, 'GET')" data-toggle="modal" data-target="#modalSecuritec"><i class="fa fa-edit" aria-hidden="true"></i></button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </template>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </template>
                        </div>
                        <div class="box-footer clearfix">
                            <div class="no-margin pull-right">
                                <div v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                                    <div v-bind:class="!initializeClientes && dataClientes.length > 0 || !initializeClientes ? '' : 'disabledDiv'">
                                        <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchClientes()"></pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{!! asset('js/vue/tableClientesVue.js?version='.time()) !!}"></script>
    <script>
        $(document).ready(function() {
            vmTableClientes.fetchClientes()
        })
    </script>
@endsection
