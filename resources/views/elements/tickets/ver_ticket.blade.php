@extends('layouts.layoutApp', ['module' => $titleModule])

@section('css')
    <style>
        .progress-fix{
            height:33px;
            text-align:center;
        }

        .progress-bar-fix {
            margin-top: 7px !important;
            margin-left: 3px !important;
        }
    </style>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="loadingTabs">
                    <div class="col-md-12">
                        <div class="cssload-container">
                            <div class="cssload-lt"></div>
                            <div class="cssload-rt"></div>
                            <div class="cssload-lb"></div>
                            <div class="cssload-rb"></div>
                        </div>
                    </div>
                </div>
                <div class="startTabs hidden">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            <li class=""><a href="#comentarios" data-toggle="tab" aria-expanded="false" onclick="vmTicketComentarios.fetchComentarios('{{ $dataTicket['id'] }}')">Comentarios</a></li>
                            <li class=""><a href="#tareas" data-toggle="tab" aria-expanded="true" onclick="vmTableTareasTicket.fetchTareasTicket('{{ $dataTicket['id'] }}')">Tareas</a></li>
                            <li class="active"><a href="#detalle" data-toggle="tab" aria-expanded="false">Detalle</a></li>
                            <li class="pull-left header"><i class="fa fa-ticket"></i> {{ $dataTicket['asunto'] }}</li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="comentarios">
                                @include('elements.tickets.tabs.comentarios')
                            </div>
                            <div class="tab-pane" id="tareas">
                                @include('elements.tickets.tabs.tareas')
                            </div>
                            <div class="tab-pane active" id="detalle">
                                @if($message = Session::get('success'))
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-check"></i> Exitoso!</h4>
                                        {{ $message }}
                                    </div>
                                @endif
                                @if($message = Session::get('error'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                        {{ $message }}
                                    </div>
                                @endif
                                @include('elements.tickets.tabs.detalle')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{!! asset('js/vue/tableTareasTicketVue.js?version='.time()) !!}"></script>
    <script src="{!! asset('js/vue/ticketComentariosVue.js?version='.time()) !!}"></script>
    <script>
        selectPicker('.selectBoostrap')
        dateTimePicker('.datePickerTicketRegistro', {
            enableTime: false,
            dateFormat: "Y-m-d",
            wrap: true,
            locale: "es"
        })
        dateTimePicker('.timePickerTicketRegistro', {
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            wrap: true,
            locale: "es",
            time_24hr: true
        })

        function customURLConverter(url, node, on_save, name) {
            url = url.replace('../../../', '/')
            return url
        }

        tinyEditor('textarea.ticketUpdate', {
            urlconverter_callback : 'customURLConverter',
            language: 'es',
            extended_valid_elements: "table[class=table table-bordered],ul[style=list-style-type: circle;],img[class=img-responsive center-block|!src|border:0|alt|title|width|height|style]",
            theme: "modern",
            height: 300,
            font_formats: "Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n",
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager smileys"
            ],
            auto_convert_smileys: true,
            paste_data_images: true,
            toolbar1: "responsivefilemanager undo redo | formatselect styleselect fontsizeselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink anchor | image table | print media smileys | fullscreen preview",
            style_formats: [
                {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
                {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
                {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
                {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
                {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
                {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
                {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
                {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
                {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
                {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
                {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
                {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
                {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
            ],
            menubar: true,
            fullscreen_new_window : true,
            toolbar_items_size: 'small',
            image_advtab: true,
            fix_list_elements : true,
            external_filemanager_path: "{{ '' }}../js/tinymce/filemanager/",
            filemanager_title: "Gestor de Archivos" ,
            external_plugins: { filemanager: "filemanager/plugin.min.js"},
            branding: false,
            setup: function (editor) {
                editor.on('init', function (e) {
                    $('.loadingTabs').addClass('hidden')
                    $('.startTabs').removeClass('hidden')
                })
            }
        })

        tinyEditor('textarea.commentUpdate', {
            urlconverter_callback : 'customURLConverter',
            language: 'es',
            extended_valid_elements: "table[class=table table-bordered],ul[style=list-style-type: circle;],img[class=img-responsive center-block|!src|border:0|alt|title|width|height|style]",
            theme: "modern",
            height: 300,
            font_formats: "Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n",
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager smileys"
            ],
            auto_convert_smileys: true,
            paste_data_images: true,
            toolbar1: "responsivefilemanager undo redo | formatselect styleselect fontsizeselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink anchor | image table | print media smileys | fullscreen preview",
            style_formats: [
                {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
                {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
                {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
                {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
                {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
                {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
                {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
                {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
                {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
                {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
                {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
                {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
                {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
            ],
            menubar: true,
            fullscreen_new_window : true,
            toolbar_items_size: 'small',
            image_advtab: true,
            fix_list_elements : true,
            external_filemanager_path: "{{ '' }}../js/tinymce/filemanager/",
            filemanager_title: "Gestor de Archivos" ,
            external_plugins: { filemanager: "filemanager/plugin.min.js"},
            branding: false,
            init_instance_callback: function (editor) {
                editor.on('KeyUp', function (e) {
                    let textoContenido = e.currentTarget.innerText
                    let contenido = e.currentTarget.innerHTML

                    if(textoContenido.length > 1){
                        vmTicketComentarios.textoComentario = contenido
                    }else{
                        vmTicketComentarios.textoComentario = ''
                    }
                })
            }
        })

        vmTicketComentarios.idUsuario = '{{ auth()->id()  }}'
    </script>
@endsection
