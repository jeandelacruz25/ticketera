<form action="{{ url('/saveFormTickets') }}" method="post" enctype="multipart/form-data">
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Progreso</label>
                    <div class="progress progress-fix">
                        <div class="progress-bar progress-bar-fix" role="progressbar" style="width: {{ $dataTicket['avance'].'%' }}" aria-valuenow="{{ $dataTicket['avance'] }}" aria-valuemin="0" aria-valuemax="100">
                            {{ $dataTicket['avance'] ? floor($dataTicket['avance']).' %' : '0%' }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Estado</label>
                    <select class="form-control selectBoostrap" name="idEstado">
                        @foreach($dataEstado as $key => $value)
                            <option value="{{ $value['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $value['color'] }}; color: #ffffff'>{{ $value['estado'] }}</button>" {{ $dataTicket['id_estado'] == $value['id'] ? 'selected' : '' }}>{{ $value['estado'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Cliente</label>
                    <select class="form-control selectBoostrap" name="selectCliente" data-live-search="true">
                        @foreach($dataCliente as $key => $value)
                            <option value="{{ $value['id'] }}" {{ $value['id'] == $dataTicket['id_cliente'] ? 'selected' : '' }}>{{ $value['cliente'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Solicitante</label>
                    <input type="text" name="solicitanteTicket" class="form-control" placeholder="Ingrese el solicitante" value="{{ $dataTicket['solicitante'] }}">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Asunto</label>
                    <input type="text" name="asuntoTicket" class="form-control" placeholder="Ingrese el asunto" value="{{ $dataTicket['asunto'] }}">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Detalle / Observación</label>
                    <textarea name="detalleTicket" class="form-control ticketUpdate" rows="3">{{ $dataTicket['detalle'] }}</textarea>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Fecha Inicio</label>
                    <input type="text" class="form-control" value="{{ $dataTicket['fecha_inicio'] ? \Carbon\Carbon::parse($dataTicket['fecha_inicio'])->format('d/m/Y H:i') : '' }}" disabled>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Fecha Cierre</label>
                    <input type="text" class="form-control" value="{{ $dataTicket['fecha_cierre'] ? \Carbon\Carbon::parse($dataTicket['fecha_cierre'])->format('d/m/Y H:i') : '' }}" disabled>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Fecha Vencimiento</label>
                    <input type="text" class="form-control" value="{{ $dataTicket['fecha_venc'] ? \Carbon\Carbon::parse($dataTicket['fecha_venc'])->format('d/m/Y H:i') : '' }}" disabled>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Fecha Registro</label>
                    <div class="input-group flatpickr datePickerTicketRegistro" data-id="strap">
                        <input type="text" class="form-control flatpickr-input" name="fechaRegistro" data-input="" value="{{ \Carbon\Carbon::parse($dataTicket['fecha_reg'])->format('Y-m-d') }}" readonly>
                        <a class="input-group-addon input-button" title="Abrir calendario" data-toggle="" style="cursor: pointer"><i class="fa fa-calendar"></i></a>
                        <a class="input-group-addon input-button" title="Limpiar fecha" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Hora Registro</label>
                    <div class="input-group flatpickr timePickerTicketRegistro" data-id="strap">
                        <input type="text" class="form-control flatpickr-input" name="horaRegistro" data-input="" value="{{ \Carbon\Carbon::parse($dataTicket['fecha_reg'])->format('H:i') }}" readonly>
                        <a class="input-group-addon input-button" title="Abrir reloj" data-toggle="" style="cursor: pointer"><i class="fa fa-clock-o"></i></a>
                        <a class="input-group-addon input-button" title="Limpiar fecha" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Tipo</label>
                    <select name="tipoTicket" class="form-control selectBoostrap">
                        @foreach($dataTipo as $key => $value)
                            <option value="{{ $value['id'] }}" {{ $value['id'] == $dataTicket['id_tipoticket'] ? 'selected' : '' }}>{{ $value['tipoticket'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Origen</label>
                    <select name="origenTicket" class="form-control selectBoostrap">
                        @foreach($dataOrigen as $key => $value)
                            <option value="{{ $value['id'] }}" {{ $value['id'] == $dataTicket['id_tipoorigen'] ? 'selected' : '' }}>{{ $value['tipo_origen'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Prioridad</label>
                    <select name="prioridadTicket" class="form-control selectBoostrap">
                        @foreach($dataPrioridad as $key => $value)
                            <option value="{{ $value['id'] }}" {{ $value['id'] == $dataTicket['id_tipoprioridad'] ? 'selected' : '' }}>{{ $value['tipo_prioridad'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Dificultad</label>
                    <select name="dificultadTicket" class="form-control selectBoostrap">
                        @foreach($dataDificultad as $key => $value)
                            <option value="{{ $value['id'] }}" {{ $value['id'] == $dataTicket['id_tipodificultad'] ? 'selected' : '' }}>{{ $value['tipo_dificultad'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Grupo</label>
                    <select name="grupoTicket" class="form-control selectBoostrap">
                        @foreach($dataGrupo as $key => $value)
                            <option value="{{ $value['id'] }}" {{ $value['id'] == $dataTicket['grupo']['id'] ? 'selected' : '' }}>{{ $value['grupo'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-danger formError" style="display: none"></div>
    <input type="hidden" name="ticketID" value="{{ $dataTicket['id'] }}">
    <input type="hidden" name="fecInicio" value="{{ $dataTicket['fecha_inicio'] }}">
    <input type="hidden" name="fecCierre" value="{{ $dataTicket['fecha_cierre'] }}">
    <input type="hidden" name="fecVenc" value="{{ $dataTicket['fecha_venc'] }}">
    <input type="hidden" name="nroTarea" value="{{ $dataTicket['nro_tareas'] }}">
    <input type="hidden" name="avance" value="{{ $dataTicket['avance'] }}">
    <input type="hidden" name="fecReg" value="{{ $dataTicket['fecha_reg'] }}">
    <input type="hidden" name="userReg" value="{{ $dataTicket['user_reg'] }}">
    <input type="hidden" name="orden" value="{{ $dataTicket['orden'] }}">
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary btnForm"><i class='fa fa-edit'></i> Editar</button>
        <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
    </div>
</form>
