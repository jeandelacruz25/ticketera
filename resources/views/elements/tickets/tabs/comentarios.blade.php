<div class="box box-danger direct-chat direct-chat-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Comentarios del Ticket</h3>
    </div>
    <!-- /.box-header -->
    <div id="ticketComentariosVue">
        <div class="box-body">
            <template v-if="initializeComentarios">
                <div class="direct-chat-messages">
                    <div class="col-md-12">
                        <div class="cssload-container">
                            <div class="cssload-lt"></div>
                            <div class="cssload-rt"></div>
                            <div class="cssload-lb"></div>
                            <div class="cssload-rb"></div>
                        </div>
                    </div>
                </div>
            </template>
            <template v-else>
                <template v-if="dataComentarios.length > 0">
                    <div class="direct-chat-messages" style="height: 380px !important;">
                        <template v-for="(dataComentario, index) in dataComentarios">
                            <div class="direct-chat-msg" :class="[dataComentario.usuario.id == idUsuario ? '' : 'right']">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name" :class="[dataComentario.usuario.id == idUsuario ? 'pull-left' : 'pull-right']" v-html="dataComentario.usuario.name"></span>
                                    <span class="direct-chat-timestamp" :class="[dataComentario.usuario.id == idUsuario ? 'pull-right' : 'pull-left']" v-html="fechaReg[index]"></span>
                                </div>
                                <img class="direct-chat-img" :src="dataAvatars[index].encoded">
                                <div class="direct-chat-text" v-html="dataComentario.comentario"></div>
                            </div>
                        </template>
                    </div>
                </template>
                <template v-else>
                    <div class="direct-chat-messages">
                        <div class="col-md-12">
                            <div class="alert alert-warning">
                                <h4><i class="icon fa fa-warning"></i> No hay ningún comentario agregado en este ticket. </h4>
                            </div>
                        </div>
                    </div>
                </template>
            </template>
        </div>
        <div class="box-footer">
            <div class="input-group">
                <div class="col-md-10">
                    <div class="form-group">
                        <textarea name="commentTicket" class="form-control commentUpdate" rows="3" v-model="textoComentario"></textarea>
                    </div>
                </div>
                <div class="col-md-2">
                    <span class="input-group-btn">
                        <template v-if="textoComentario.length > 0 && !loadSubmit">
                            <button type="button" class="btn btn-danger btn-flat" @click="guardarComentario">Comentar</button>
                        </template>
                        <template v-else-if="loadSubmit">
                            <button type="button" class="btn btn-danger btn-flat disabled" disabled><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                        </template>
                        <template v-else-if="textoComentario.length <= 0 && !loadSubmit">
                            <button type="button" class="btn btn-danger btn-flat disabled" disabled>Comentar</button>
                        </template>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
