<div class="box-body table-responsive no-padding">
    <div class="pull-left">
        <div class="btn btn-group">
            <a onclick="responseModalChangeUser('div.dialogSecuritec','/formChangeUsers', '{{ $dataTicket['id'] }}', 'POST')" data-toggle="modal" data-target="#modalSecuritec" class="btn btn-info"><i class="fa fa-refresh" aria-hidden="true" ></i> Cambiar Usuarios</a>
        </div>
    </div>
    <div class="pull-right">
        <div class="btn btn-group">
            <a onclick="responseModal('div.dialogSecuritecLarge','/formTareas', { idTicket: '{{ $dataTicket['id'] }}', idTarea: '', ticketTask: false }, 'POST')" data-toggle="modal" data-target="#modalSecuritec" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Agregar Tarea</a>
        </div>
    </div>
    <div id="tableTareasTicketVue">
        <div v-if="initializeTareas">
            <div class="col-md-12">
                <div class="cssload-container">
                    <div class="cssload-lt"></div>
                    <div class="cssload-rt"></div>
                    <div class="cssload-lb"></div>
                    <div class="cssload-rb"></div>
                </div>
            </div>
        </div>
        <template v-else-if="!initializeTareas && dataTareas.length === 0 && !initDataPagination">
            <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th><span class="align-middle">ID</span></th>
                                <th>Tarea</th>
                                <th>Usuario</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Cierre</th>
                                <th>Fecha Vencimiento</th>
                                <th>Estado Dev</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td colspan="8">No hay ningúna tarea registrada actualmente.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="align-middle checkGeneral" onclick="markCheckAll('.checkGeneral', '.checkChildren')">
                                    <span class="align-middle">ID</span>
                                </th>
                                <th>Tarea</th>
                                <th>Usuario</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Cierre</th>
                                <th>Fecha Vencimiento</th>
                                <th>Estado Dev</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataTarea, index) in dataTareas">
                                <tr>
                                    <td>
                                        <input type="checkbox" name="changeUser[]" class="align-middle checkChildren" v-bind:value="dataTarea.id"> <span class="align-middle" v-html="pagination.from + index"></span>
                                    </td>
                                    <td>
                                        <a data-tooltip="tooltip" title="" v-bind:data-original-title="dataTarea.tarea" data-placement="right" style="cursor: pointer" @click="clickEvent('div.dialogSecuritecLarge','/formTareas', { idTicket: dataTarea.ticket.id, idTarea: dataTarea.id, ticketTask: true }, 'POST')" data-toggle="modal" data-target="#modalSecuritec" v-html="dataTarea.tarea"></a>
                                    </td>
                                    <td v-html="dataTarea.usuario.name"></td>
                                    <td v-html="fechaInicio[index]"></td>
                                    <td v-html="fechaCierre[index]"></td>
                                    <td v-html="fechaVenc[index]"></td>
                                    <td>
                                        <button class="btn btn-xs" v-bind:style="{ 'background-color': dataTarea.env.color, 'color': 'white' }" v-html="dataTarea.env.estado"></button>
                                    </td>
                                    <td class="text-center" v-bind:style="{'background-color': dataTarea.estado.color, 'color': 'white'}" v-html="dataTarea.estado.estado"></td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <div class="no-margin pull-right">
            <div v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                <div v-bind:class="!initializeTareas && dataTareas.length > 0 || !initializeTareas ? '' : 'disabledDiv'">
                    <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchTareasTicket()"></pagination>
                </div>
            </div>
        </div>
    </div>
</div>
