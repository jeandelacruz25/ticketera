@extends('layouts.layoutApp', ['module' => $titleModule])

@section('content')
    <section class="content-header">
        <h1>
            {{ $titleModule }}
            <small>{{ $titleSubModule }}</small>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div id="tableTicketsVue">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $titleBox }}</h3>
                            <div class="box-tools">
                                <div class="btn-group pull-right">
                                    <a onclick="responseModal('div.dialogSecuritecLarge','formTickets', {}, 'GET')" data-toggle="modal" data-target="#modalSecuritec" class="btn btn-primary btn-xs">
                                        <i class="fa fa-ticket" aria-hidden="true"></i> Agregar Ticket
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div v-if="initializeTickets">
                                <div class="col-md-12">
                                    <div class="cssload-container">
                                        <div class="cssload-lt"></div>
                                        <div class="cssload-rt"></div>
                                        <div class="cssload-lb"></div>
                                        <div class="cssload-rb"></div>
                                    </div>
                                </div>
                            </div>
                            <template v-else-if="!initializeTickets && dataTickets.length === 0 && !initDataPagination">
                                <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                                    <div class="col-md-12">
                                        <div class="box-tools">
                                            <div class="col-md-3">
                                                <select class="form-control selectBoostrap" data-live-search="true" v-model="selectCliente" @change="fetchTickets()">
                                                    <option value="" selected>Seleccione el cliente</option>
                                                    @foreach($dataCliente as $key => $valueCliente)
                                                        <option value="{{ $valueCliente['id'] }}">{{ $valueCliente['cliente'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control selectBoostrap" v-model="selectTipo" @change="fetchTickets()">
                                                    @foreach($dataTipo as $key => $valueTipo)
                                                        <option value="{{ $valueTipo['id'] }}">{{ $valueTipo['tipoticket'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control selectBoostrap" data-live-search="true" v-model="selectEstado" @change="fetchTickets()" data-selected-text-format="count > 0" multiple>
                                                    @foreach($dataEstado as $key => $valueEstado)
                                                        <option value="{{ $valueEstado['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $valueEstado['color'] }}; color: #ffffff'>{{ $valueEstado['estado'] }}</button>">{{ $valueEstado['estado'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group input-group-sm">
                                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar Ticket" v-model="searchTicket" @keyup.enter="fetchTickets()">
                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btn-primary" @click="fetchTickets()"><i class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="col-md-12 table-responsive table-sm">
                                        <table class="table table-bordered table-hover table-sm">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Cliente</th>
                                                    <th>Asunto</th>
                                                    <th>Tipo</th>
                                                    <th>Fec.Ven</th>
                                                    <th>Avance</th>
                                                    <th>Estado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="text-center">
                                                    <td colspan="8">No hay ningún tickets registrado actualmente.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </template>
                            <template v-else>
                                <div class="" v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                                    <div class="col-md-12">
                                        <div class="box-tools">
                                            <div class="col-md-3">
                                                <select class="form-control selectBoostrap" data-live-search="true" v-model="selectCliente" @change="fetchTickets()">
                                                    <option value="" selected>Seleccione el cliente</option>
                                                    @foreach($dataCliente as $key => $valueCliente)
                                                        <option value="{{ $valueCliente['id'] }}">{{ $valueCliente['cliente'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control selectBoostrap" v-model="selectTipo" @change="fetchTickets()">
                                                    <option value="" selected>Seleccione el tipo</option>
                                                    @foreach($dataTipo as $key => $valueTipo)
                                                        <option value="{{ $valueTipo['id'] }}">{{ $valueTipo['tipoticket'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control selectBoostrap" data-live-search="true" v-model="selectEstado" @change="paginationFetchTickets()" data-selected-text-format="count > 0" multiple>
                                                    @foreach($dataEstado as $key => $valueEstado)
                                                        <option value="{{ $valueEstado['id'] }}" data-content="<button class='btn btn-xs btn-block' style='background: {{ $valueEstado['color'] }}; color: #ffffff'>{{ $valueEstado['estado'] }}</button>">{{ $valueEstado['estado'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group input-group-sm">
                                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar Ticket" v-model="searchTicket" @keyup.enter="fetchTickets()">
                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btn-primary" @click="fetchTickets()"><i class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="col-md-12">
                                        <div class="pull-right">
                                            <button class="btn btn-success btn-sm" @click="clickDownload()"><i class="fa fa-file-excel-o"></i> Descargar Reporte</button>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered table-hover table-sm">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Cliente</th>
                                                    <th>Asunto</th>
                                                    <th>Tipo</th>
                                                    <th>Fec.Ven</th>
                                                    <th>Avance</th>
                                                    <th>Peso</th>
                                                    <th>Tasks</th>
                                                    <th>Estado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <template v-for="(dataTicket, index) in dataTickets">
                                                    <tr style="height:2px;">
                                                        <td v-html="pagination.from + index"></td>
                                                        <td v-html="dataTicket.cliente.cliente"></td>
                                                        <td>
                                                            <a data-tooltip="tooltip" title="" v-bind:data-original-title="dataTicket.asunto" data-placement="right" v-bind:href="'/editTicket/' + dataTicket.id" style="cursor: pointer" v-html="nameAsunto[index]">
                                                            </a>
                                                        </td>
                                                        <td v-html="dataTicket.tipo.tipoticket"></td>
                                                        <td v-html="fechaVenc[index]"></td>
                                                        <td>
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" v-bind:style="{ width: dataTicket.avance + '%' }" v-bind:aria-valuenow="dataTicket.avance" aria-valuemin="0" aria-valuemax="100" v-html="parseFloat(dataTicket.avance).toFixed(0) + '%'"></div>
                                                            </div>
                                                        </td>
                                                        <td v-html="dataTicket.orden"></td>
                                                        <td v-html="dataTicket.tareas_count"></td>
                                                        <td class="text-center" v-bind:style="{'background-color': dataTicket.estado.color, 'color': 'white'}" v-html="dataTicket.estado.estado"></td>
                                                    </tr>
                                                </template>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </template>
                        </div>
                        <div class="box-footer clearfix">
                            <div class="no-margin pull-right">
                                <div v-bind:class="!initializeLoadPaginate ? 'disabledDiv' : ''">
                                    <div v-bind:class="!initializeTickets && dataTickets.length > 0 || !initializeTickets ? '' : 'disabledDiv'">
                                        <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchTickets()"></pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{!! asset('js/vue/tableTicketsVue.js?version='.time()) !!}"></script>
    <script>
        $(document).ready(function() {
            vmTableTickets.fetchTickets()
        })
    </script>
@endsection
