<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar usuarios de {{ $checkUsuarios ? count($checkUsuarios) : 0 }} {{ $checkUsuarios ? count($checkUsuarios) > 1 ? 'tareas' : 'tarea' : 'tarea' }}</h4>
    </div>
    <div class="modal-body">
        <form id="formChangeUsers">
            @if($checkUsuarios)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Asignar al usuario : </label>
                        <select class="form-control selectBoostrap" name="selectUsuarios" data-live-search="true">
                            <option value="" selected disabled>-</option>
                            @foreach($dataUsuarios as $key => $value)
                                <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="alert alert-danger formError" style="display: none"></div>
            <input type="hidden" name="ticketID" value="{{ $dataTicket['id'] }}">
            @foreach($checkUsuarios as $key => $value)
                <input type="hidden" name="taskID[]" value="{{ $value }}">
            @endforeach
            @else
                <div class="alert alert-danger formError">
                    No se selecciono ninguna tarea, favor de seleccionar una
                </div>
            @endif
            <div class="modal-footer">
                @if($checkUsuarios)
                    <button type="submit" class="btn btn-primary btnForm"><i class='fa fa-refresh'></i> Actualizar</button>
                    <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                @endif
                <button type="button" class="btn btn-default" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formTickets.js?version='.date('YmdHis')) !!}"></script>
<script>
    $('.selectBoostrap').selectpicker();
    hideErrorForm('.formError')
    clearModalClose('modalSecuritec', 'div.dialogSecuritec')
</script>
