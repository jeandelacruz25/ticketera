<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritecLarge')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ $updateForm ? "Editar" : "Agregar" }} Ticket</h4>
    </div>
    <div class="modal-body">
        <div class="loadingFormTicket">
            <div class="row">
                <div class="col-md-12">
                    <div class="cssload-container">
                        <div class="cssload-lt"></div>
                        <div class="cssload-rt"></div>
                        <div class="cssload-lb"></div>
                        <div class="cssload-rb"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="startFormTicket hidden">
            <form id="formTicket">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cliente</label>
                            <select class="form-control selectBoostrap" name="selectCliente" data-live-search="true">
                                @if(!$updateForm) <option value="" selected disabled>-</option> @endif
                                @foreach($dataCliente as $key => $value)
                                    <option value="{{ $value['id'] }}" @if($updateForm) {{ $value['id'] == $dataTicket['id_cliente'] ? 'selected' : '' }} @endif>{{ $value['cliente'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Solicitante</label>
                            <input type="text" name="solicitanteTicket" class="form-control" placeholder="Ingrese el solicitante" value="{{ $updateForm ? $dataTicket['solicitacion'] : '' }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Asunto</label>
                            <input type="text" name="asuntoTicket" class="form-control" placeholder="Ingrese el asunto" value="{{ $updateForm ? $dataTicket['asunto'] : '' }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Detalle / Observación</label>
                            <textarea name="detalleTicket" class="form-control ticketEditor" rows="3"> {{ $updateForm ? $dataTicket['detalle'] : '' }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Fecha Registro</label>
                            <div class="input-group flatpickr datePickerTicket" data-id="strap">
                                <input type="text" class="form-control flatpickr-input" name="fechaRegistro" data-input="" value="{{ $updateForm ? \Carbon\Carbon::parse($dataTicket['fecha_reg'])->format('Y-m-d') : '' }}" readonly>
                                <a class="input-group-addon input-button" title="Abrir calendario" data-toggle="" style="cursor: pointer"><i class="fa fa-calendar"></i></a>
                                <a class="input-group-addon input-button" title="Limpiar fecha" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Hora Registro</label>
                            <div class="input-group flatpickr timePickerTicket" data-id="strap">
                                <input type="text" class="form-control flatpickr-input" name="horaRegistro" data-input="" value="{{ $updateForm ? \Carbon\Carbon::parse($dataTicket['fecha_reg'])->format('H:i') : '' }}" readonly>
                                <a class="input-group-addon input-button" title="Abrir reloj" data-toggle="" style="cursor: pointer"><i class="fa fa-clock-o"></i></a>
                                <a class="input-group-addon input-button" title="Limpiar fecha" data-clear="" style="cursor: pointer"><i class="fa fa-eraser text-danger"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tipo</label>
                            <select name="tipoTicket" class="form-control selectBoostrap">
                                @if(!$updateForm) <option value="" selected disabled>-</option> @endif
                                @foreach($dataTipo as $key => $value)
                                    <option value="{{ $value['id'] }}" @if($updateForm) {{ $value['id'] == $dataTicket['id_tipoticket'] ? 'selected' : '' }} @endif>{{ $value['tipoticket'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Origen</label>
                            <select name="origenTicket" class="form-control selectBoostrap">
                                @if(!$updateForm) <option value="" selected disabled>-</option> @endif
                                @foreach($dataOrigen as $key => $value)
                                    <option value="{{ $value['id'] }}" @if($updateForm) {{ $value['id'] == $dataTicket['id_tipoorigen'] ? 'selected' : '' }} @endif>{{ $value['tipo_origen'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Prioridad</label>
                            <select name="prioridadTicket" class="form-control selectBoostrap">
                                @if(!$updateForm) <option value="" selected disabled>-</option> @endif
                                @foreach($dataPrioridad as $key => $value)
                                    <option value="{{ $value['id'] }}" @if($updateForm) {{ $value['id'] == $dataTicket['id_tipoprioridad'] ? 'selected' : '' }} @endif>{{ $value['tipo_prioridad'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Dificultad</label>
                            <select name="dificultadTicket" class="form-control selectBoostrap">
                                @if(!$updateForm) <option value="" selected disabled>-</option> @endif
                                @foreach($dataDificultad as $key => $value)
                                    <option value="{{ $value['id'] }}" @if($updateForm) {{ $value['id'] == $dataTicket['id_tipodificultad'] ? 'selected' : '' }} @endif>{{ $value['tipo_dificultad'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Grupo</label>
                            <select name="grupoTicket" class="form-control selectBoostrap">
                                @if(!$updateForm) <option value="" selected disabled>-</option> @endif
                                @foreach($dataGrupo as $key => $value)
                                    <option value="{{ $value['id'] }}" @if($updateForm) {{ $value['id'] == $dataTicket['grupo']['id'] ? 'selected' : '' }} @endif>{{ $value['grupo'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger formError" style="display: none"></div>
                <input type="hidden" name="ticketID" value="{{ $updateForm ? $dataTicket['id'] : '' }}">
                <input type="hidden" name="fecInicio" value="{{ $updateForm ? $dataTicket['fecha_inicio'] : '' }}">
                <input type="hidden" name="fecCierre" value="{{ $updateForm ? $dataTicket['fecha_cierre'] : '' }}">
                <input type="hidden" name="fecVenc" value="{{ $updateForm ? $dataTicket['fecha_venc'] : '' }}">
                <input type="hidden" name="nroTarea" value="{{ $updateForm ? $dataTicket['nro_tareas'] : '' }}">
                <input type="hidden" name="avance" value="{{ $updateForm ? $dataTicket['avance'] : '' }}">
                <input type="hidden" name="idEstado" value="{{ $updateForm ? $dataTicket['id_estado'] : '' }}">
                <input type="hidden" name="fecReg" value="{{ $updateForm ? $dataTicket['fecha_reg'] : '' }}">
                <input type="hidden" name="userReg" value="{{ $updateForm ? $dataTicket['user_reg'] : '' }}">
                <input type="hidden" name="orden" value="{{ $updateForm ? $dataTicket['orden'] : '' }}">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btnForm">{!! $updateForm ? "<i class='fa fa-edit'></i> Editar" : "<i class='fa fa-plus'></i> Agregar" !!}</button>
                    <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                    <button type="button" class="btn btn-default" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formTickets.js?version='.date('YmdHis')) !!}"></script>
<script>
    selectPicker('.selectBoostrap')
    dateTimePicker('.datePickerTicket', {
        enableTime: false,
        dateFormat: "Y-m-d",
        wrap: true,
        locale: "es"
    })
    dateTimePicker('.timePickerTicket', {
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        wrap: true,
        locale: "es",
        time_24hr: true
    })

    function customURLConverter(url, node, on_save, name) {
        url = url.replace('../../../', '/')
        return url
    }

    tinyEditor('textarea.ticketEditor', {
        urlconverter_callback : 'customURLConverter',
        language: 'es',
        extended_valid_elements: "table[class=table table-bordered],ul[style=list-style-type: circle;],img[class=img-responsive center-block|!src|border:0|alt|title|width|height|style]",
        theme: "modern",
        height: 300,
        font_formats: "Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n",
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager smileys"
        ],
        auto_convert_smileys: true,
        paste_data_images: true,
        toolbar1: "responsivefilemanager undo redo | formatselect styleselect fontsizeselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink anchor | image table | print media smileys | fullscreen preview",
        style_formats: [
            {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
            {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
            {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
            {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
            {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
            {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
            {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
            {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
            {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
            {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
            {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
            {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
            {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
        ],
        menubar: true,
        fullscreen_new_window : true,
        toolbar_items_size: 'small',
        image_advtab: true,
        fix_list_elements : true,
        external_filemanager_path: "{{ '' }}../js/tinymce/filemanager/",
        filemanager_title: "Gestor de Archivos" ,
        external_plugins: { filemanager: "filemanager/plugin.min.js"},
        branding: false,
        setup: function (editor) {
            editor.on('init', function (e) {
                $('.loadingFormTicket').addClass('hidden')
                $('.startFormTicket').removeClass('hidden')
            })
        }
    })
    hideErrorForm('.formError')
    clearModalClose('modalSecuritec', 'div.dialogSecuritecLarge')
</script>
