<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ env('APP_NAME', 'Securitec Perú') }}</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon -->
        <link rel="shortcut icon" href="{!! asset('img/favicon.png?version='.time()) !!}" type="image/x-icon">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{!! asset('bower_components/bootstrap/dist/css/bootstrap.min.css?version='.time()) !!}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{!! asset('bower_components/font-awesome/css/font-awesome.min.css?version='.time()) !!}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{!! asset('bower_components/Ionicons/css/ionicons.min.css?version='.time()) !!}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{!! asset('css/AdminLTE.min.css?version='.time()) !!}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{!! asset('css/skins/_all-skins.min.css?version='.time()) !!}">
        <!-- Morris chart -->
        <link rel="stylesheet" href="{!! asset('bower_components/morris.js/morris.css?version='.time()) !!}">
        <!-- jvectormap -->
        <link rel="stylesheet" href="{!! asset('bower_components/jvectormap/jquery-jvectormap.css?version='.time()) !!}">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{!! asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css?version='.time()) !!}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{!! asset('bower_components/bootstrap-daterangepicker/daterangepicker.css?version='.time()) !!}">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="{!! asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css?version='.time()) !!}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.1/flatpickr.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.1/plugins/confirmDate/confirmDate.css">
        <link rel="stylesheet" href="{!! asset('css/securitec.css?version='.time()) !!}">
        @yield('css')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

      <!-- Google Font -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-red sidebar-mini fixed">
        <div class="wrapper">
            @include('layouts.utils.header')
            @include('layouts.utils.sidebar', ['module' => $module])
            <div class="content-wrapper">
                @yield('content')
            </div>
            @include('layouts.utils.footer')
            @include('layouts.utils.control_sidebar')
            @include('layouts.utils.modals')
            <div class="control-sidebar-bg"></div>
        </div>

        <!-- jQuery 3 -->
        <script src="{!! asset('bower_components/jquery/dist/jquery.min.js?version='.time()) !!}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{!! asset('bower_components/jquery-ui/jquery-ui.min.js?version='.time()) !!}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{!! asset('bower_components/bootstrap/dist/js/bootstrap.min.js?version='.time()) !!}"></script>
        <!-- Morris.js charts -->
        <script src="{!! asset('bower_components/raphael/raphael.min.js?version='.time()) !!}"></script>
        <script src="{!! asset('bower_components/morris.js/morris.min.js?version='.time()) !!}"></script>
        <!-- Sparkline -->
        <script src="{!! asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js?version='.time()) !!}"></script>
        <!-- jvectormap -->
        <script src="{!! asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js?version='.time()) !!}"></script>
        <script src="{!! asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js?version='.time()) !!}"></script>
        <!-- jQuery Knob Chart -->
        <script src="{!! asset('bower_components/jquery-knob/dist/jquery.knob.min.js?version='.time()) !!}"></script>
        <!-- daterangepicker -->
        <script src="{!! asset('bower_components/moment/min/moment.min.js?version='.time()) !!}"></script>
        <script src="{!! asset('bower_components/bootstrap-daterangepicker/daterangepicker.js?version='.time()) !!}"></script>
        <!-- datepicker -->
        <script src="{!! asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?version='.time()) !!}"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{!! asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js?version='.time()) !!}"></script>
        <!-- Slimscroll -->
        <script src="{!! asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js?version='.time()) !!}"></script>
        <!-- FastClick -->
        <script src="{!! asset('bower_components/fastclick/lib/fastclick.js?version='.time()) !!}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/i18n/defaults-es_ES.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.1/flatpickr.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.1/plugins/confirmDate/confirmDate.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.1/l10n/es.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/es.js"></script>
        <script src="{!! asset('js/tinymce/tinymce.min.js?version='.time()) !!}"></script>
        <script src="{!! asset('js/tinymce/jquery.tinymce.min.js?version='.time()) !!}"></script>
        <!-- AdminLTE App -->
        <script src="{!! asset('js/adminlte.min.js?version='.time()) !!}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{!! asset('js/demo.js?version='.time()) !!}"></script>
        <script src="{!! asset('js/app.js?version='.time()) !!}"></script>
        <script src="{!! asset('js/helper.js?version='.time()) !!}"></script>
        <script src="{!! asset('js/nodeClient.js?version='.time()) !!}"></script>
        <script src="{!! asset('js/nodeTicketera.js?version='.time()) !!}"></script>
        @yield('script')
    </body>
</html>
