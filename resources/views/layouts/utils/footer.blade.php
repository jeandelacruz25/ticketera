<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; {{ \Jenssegers\Date\Date::now()->format('Y') }} <a href="https://securitec.pe">Securitec Perú</a>.</strong> Todos los derechos reservados.
</footer>
