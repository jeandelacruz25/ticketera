<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Avatar::create(auth()->user()->name)->setDimension(160, 160)->toBase64() }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->username }}</p>
                <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENÚ NAVEGACIÓN</li>
            @if(auth()->user()->id_rol == 1)
                <li class="{{ $module == 'Clientes' ? 'active' : '' }}">
                    <a href="/clientes">
                        <i class="fa fa-group"></i> <span>Clientes</span>
                    </a>
                </li>
            @endif
            @if(auth()->user()->id_rol == 1)
                <li class="{{ $module == 'Tickets' ? 'active' : '' }}">
                    <a href="/tickets">
                        <i class="fa fa-ticket"></i> <span>Tickets</span>
                    </a>
                </li>
            @endif
            <li class="{{ $module == 'Tareas' ? 'active' : '' }}">
                <a href="/tareas">
                    <i class="fa fa-tasks"></i> <span>Tareas</span>
                </a>
            </li>
            <!-- <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                    <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                </ul>
            </li>
            <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li> -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
