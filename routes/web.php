<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',         'HomeController@index')->name('home');

Auth::routes();

Route::get('/home',                                     'HomeController@index')->name('home');
Route::get('/clientes',                                 'ClientesController@index')->name('clientes');
Route::get('/paginationClientes',                       'ClientesController@paginationClientes')->name('paginationClientes');
Route::match(['get', 'post'],   '/formClientes',        'ClientesController@formClientes')->name('formClientes');
Route::post('/saveformClientes',                        'ClientesController@saveformClientes')->name('saveformClientes');

Route::get('/tickets',                                  'TicketsController@index')->name('tickets');
Route::get('/paginationTickets',                        'TicketsController@paginationTickets')->name('paginationTickets');
Route::get('/getTicketsComentarios',                    'TicketsController@getTicketsComentarios')->name('getTicketsComentarios');
Route::post('/saveTicketsComentarios',                  'TicketsController@saveTicketsComentarios')->name('saveTicketsComentarios');
Route::match(['get', 'post'],   '/formTickets',         'TicketsController@formTickets')->name('formTickets');
Route::get('/editTicket/{id}',                          'TicketsController@editTicket')->name('editTicket');
Route::post('/saveFormTickets',                         'TicketsController@saveFormTickets')->name('saveFormTickets');
Route::post('/downloadReportTicket',                    'TicketsController@downloadReportTicket')->name('downloadReportTicket');
Route::get('/getAvatarUsuario',                         'TicketsController@getAvatarUsuario')->name('getAvatarUsuario');

Route::get('/tareas',                                   'TareasController@index')->name('tareas');
Route::get('/paginationTareas',                         'TareasController@paginationTareas')->name('paginationTareas');
Route::get('/paginationTareasTicket',                   'TareasController@paginationTareasTicket')->name('paginationTareasTicket');
Route::match(['get', 'post'],   '/formTareas',          'TareasController@formTareas')->name('formTareas');
Route::post('/saveFormTareas',                          'TareasController@saveFormTareas')->name('saveFormTareas');
Route::post('/formChangeStatusTask',                    'TareasController@formChangeStatusTask')->name('formChangeStatusTask');
Route::post('/saveFormChangeStatusTask',                'TareasController@saveFormChangeStatusTask')->name('saveFormChangeStatusTask');
Route::match(['get', 'post'],   '/formChangeUsers',     'TicketsController@formChangeUsers')->name('formChangeUsers');
Route::post('/saveFormChangeUser',                      'TicketsController@saveFormChangeUser')->name('saveFormChangeUser');
Route::post('/formViewTicket',                          'TareasController@formViewTicket')->name('formViewTicket');
Route::post('/downloadReportTareas',                    'TareasController@downloadReportTareas')->name('downloadReportTareas');
