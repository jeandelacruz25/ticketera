var serverNodeTicketera = "https://nodeticketera.securitec.pe/";

var socketNodejs = io.connect(serverNodeTicketera, {
    reconnection: true,
    reconnectionAttempts: 15,
    reconnectionDelay: 9000,
    reconnectionDelayMax: 9000,
    secure: false
});

socketNodejs.on('connect', function () {
    console.log('socketNodejs connected!');
});

socketNodejs.on('connect_error', function () {
    swal.close();
    var timeInterval = 9;
    var refreshIntervalId = setInterval(function () {
        alertaSimple('', 'Fallo la conexi\xF3n con el socket del Server NodeJS volveremos a reintentar en ' + timeInterval + ' segundos!!!', 'error', null, true, true, true);
        timeInterval--;
        if (timeInterval === 0) clearInterval(refreshIntervalId);
    }, 1000);
    console.log('socketNodejs Connection Failed');
});

socketNodejs.on('disconnect', function () {
    alertaSimple('', 'Acabas de perder conexión con el socket del Server Nodejs !!!', 'error', null, true, true, true);
    console.log('socketNodejs Disconnected');
});

socketNodejs.on('reloadTableTareas', function () {
    if (typeof vmTableTareas !== 'undefined') vmTableTareas.nodeFetchTareas();
    console.log('[socketNodejs] Se actualizaron las tareas');
});

socketNodejs.on('reloadComentarios', function () {
    if (typeof vmTicketComentarios !== 'undefined') vmTicketComentarios.fetchComentariosRefresh();
    console.log('[socketNodejs] Se actualizaron los comentarios');
});
