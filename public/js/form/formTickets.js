$('#formTicket').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : '/saveFormTickets',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                let action = (data.action === 'create' ? 'agrego' : 'edito')
                alertaSimple('', 'Se '+ action +' correctamente el ticket', 'success', 2000)
                if(typeof vmTableTickets !== 'undefined') vmTableTickets.paginationFetchTicketsSearch(data.nameTicket)
                clearModal('modalSecuritec', 'div.dialogSecuritecLarge')
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})

$('#formChangeUsers').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : '/saveFormChangeUser',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                alertaSimple('', 'Se cambiaron los usuarios con exito', 'success', 2000)
                if(typeof vmTableTareas !== 'undefined') vmTableTareas.paginationFetchTareas()
                clearModal('modalSecuritec', 'div.dialogSecuritec')
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})
