$('#formTareas').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : '/saveFormTareas',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            console.dir(data)
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                let action = (data.action === 'create' ? 'agrego' : 'edito')
                alertaSimple('', 'Se '+ action +' correctamente la tarea', 'success', 2000)
                if(data.action === 'create'){
                    if(typeof vmTableTareasTicket !== 'undefined') vmTableTareasTicket.paginationFetchTareasTicket()
                }else if(data.action === 'update' && data.ticketTask === 'true'){
                    if(typeof vmTableTareasTicket !== 'undefined') vmTableTareasTicket.paginationFetchTareasTicket()
                }else{
                    if(typeof vmTableTareas !== 'undefined') vmTableTareas.paginationFetchTareas()
                }
                clearModal('modalSecuritec', 'div.dialogSecuritecLarge')
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})

$('#formChangeStatus').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : '/saveFormChangeStatusTask',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                let action = (data.action === 'create' ? 'agrego' : 'edito')
                alertaSimple('', 'Se actualizo el status correspondiente la tarea', 'success', 2000)
                if(typeof vmTableTareasUsuario !== 'undefined') vmTableTareasUsuario.fetchTareas()
                clearModal('modalSecuritec', 'div.dialogSecuritecLarge')
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})
