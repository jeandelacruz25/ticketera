var objComentariosTickets = {
    dataComentarios: [],
    dataAvatars: [],
    initializeComentarios: true,
    idTicket: '',
    textoComentario: '',
    loadSubmit: false,
    idUsuario: ''
}

var vmTicketComentarios = new Vue({
    el: '#ticketComentariosVue',
    data: objComentariosTickets,
    computed: {
        fechaReg() {
            if (this.dataComentarios.length > 0) {
                return this.dataComentarios.map(function (item) {
                    let fechaReg = item.fecha_reg
                    return  moment(fechaReg).isValid() ? moment(fechaReg).format('DD MMM YY hh:mm a') : '-'
                })
            }
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        fetchComentarios: async function (idTicket) {
            this.initializeComentarios = true
            const response = await this.sendUrlRequest(`/getTicketsComentarios`, {
                params: {
                    idTicket: idTicket
                }
            }, 'get')
            this.idTicket = idTicket
            this.dataComentarios = response.listComentarios
            this.dataAvatars = response.listAvatar
            this.initializeComentarios = false
        },
        fetchComentariosRefresh: async function () {
            const response = await this.sendUrlRequest(`/getTicketsComentarios`, {
                params: {
                    idTicket: this.idTicket
                }
            }, 'get')
            this.dataComentarios = response.listComentarios
            this.dataAvatars = response.listAvatar
        },
        guardarComentario: async function () {
            const textComentario = this.textoComentario
            this.loadSubmit = true
            const response = await this.sendUrlRequest(`/saveTicketsComentarios`, {
                idTicket: this.idTicket,
                comentario: textComentario
            }, 'post')
            this.loadSubmit = false
            if(response.message == 'Success'){
                tinymce.activeEditor.setContent('')
                this.textoComentario = ''
                this.fetchComentariosRefresh()
            }else{
                alertaSimple('', 'Error al insertar el comentario', 'error')
            }
        }
    }
})
