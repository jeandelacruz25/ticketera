var objTableTickets = {
    dataTickets: [],
    pagination: {
        current_page: 1
    },
    initializeTickets: true,
    initializeLoadPaginate: true,
    initDataPagination: false,
    searchTicket: '',
    selectCliente: '',
    selectTipo: '',
    selectEstado: [1, 2, 3 ,4]
}

var vmTableTickets = new Vue({
    el: '#tableTicketsVue',
    data: objTableTickets,
    computed: {
        nameAsunto() {
            if(this.dataTickets.length > 0){
                return this.dataTickets.map(function (item) {
                    let nameAsunto = item.asunto
                    return nameAsunto.length > 48 ? nameAsunto.substring(0,48)+'..' : nameAsunto
                })
            }
        },
        fechaVenc() {
            if (this.dataTickets.length > 0) {
                return this.dataTickets.map(function (item) {
                    let fechVenc = item.fecha_venc
                    return  moment(fechVenc).isValid() ? moment(fechVenc).format('DD/MM/YY HH:mm') : '-'
                })
            }
        },
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        utilsTable: function () {
            setTimeout(function() {
                $('.selectBoostrap').selectpicker({
                    style: "btn-default"
                })
                $('[data-tooltip="tooltip"]').tooltip()
            }, 100)
        },
        fetchTickets: async function () {
            this.initializeTickets = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationTickets`, {
                params: {
                    page: this.pagination.current_page,
                    searchTicket: this.searchTicket,
                    selectCliente: this.selectCliente,
                    selectTipo: this.selectTipo,
                    selectEstado: this.selectEstado,
                }
            }, 'get')
            this.dataTickets = response.data.data
            this.pagination = response.pagination
            this.initializeTickets = false
            this.searchTicket = ''
            this.utilsTable()
        },
        paginationFetchTicketsSearch: async function (nameTicket) {
            if(this.dataTickets.length === 0){
                this.initializeTickets = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationTickets`, {
                params: {
                    page: this.pagination.current_page,
                    searchTicket: nameTicket,
                    selectCliente: this.selectCliente,
                    selectTipo: this.selectTipo,
                    selectEstado: this.selectEstado,
                }
            }, 'get')
            this.dataTickets = response.data.data
            this.pagination = response.pagination

            if(this.dataTickets.length > 0){
                this.initDataPagination = true
            }else{
                this.initDataPagination = false
            }

            this.initializeLoadPaginate = true
            this.initializeTickets = false
            this.utilsTable()
        },
        paginationFetchTickets: async function () {
            if(this.dataTickets.length === 0){
                this.initializeTickets = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationTickets`, {
                params: {
                    page: this.pagination.current_page,
                    searchTicket: this.searchTicket,
                    selectCliente: this.selectCliente,
                    selectTipo: this.selectTipo,
                    selectEstado: this.selectEstado,
                }
            }, 'get')
            this.dataTickets = response.data.data
            this.pagination = response.pagination

            if(this.dataTickets.length > 0){
                this.initDataPagination = true
            }else{
                this.initDataPagination = false
            }

            this.initializeLoadPaginate = true
            this.initializeTickets = false
            this.utilsTable()
        },
        clickEvent: function (divModal, routeLoad, id, typeRequest) {
            return responseModal(divModal, routeLoad, id, typeRequest)
        },
        clickDownload: function (){
            return ajaxDownload('/downloadReportTicket', {
                searchTicket: this.searchTicket,
                selectCliente: this.selectCliente,
                selectTipo: this.selectTipo,
                selectEstado: this.selectEstado,
            })
        }
    }
})
