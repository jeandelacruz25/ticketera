var objTableClientes = {
    dataClientes: [],
    pagination: {
        current_page: 1
    },
    initializeClientes: true,
    initializeLoadPaginate: true,
    initDataPagination: false,
    searchCliente: ''
}

var vmTableClientes = new Vue({
    el: '#tableClientesVue',
    data: objTableClientes,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        fetchClientes: async function () {
            this.initializeClientes = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationClientes`, {
                params: {
                    page: this.pagination.current_page,
                    searchCliente: this.searchCliente
                }
            }, 'get')
            this.dataClientes = response.data.data
            this.pagination = response.pagination
            this.initializeClientes = false
        },
        paginationFetchClientes: async function () {
            if(this.dataClientes.length === 0){
                this.initializeClientes = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationClientes`, {
                params: {
                    page: this.pagination.current_page,
                    searchCliente: this.searchCliente
                }
            }, 'get')
            this.dataClientes = response.data.data
            this.pagination = response.pagination

            if(this.dataClientes.length > 0){
                this.initDataPagination = true
            }else{
                this.initDataPagination = false
            }

            this.initializeLoadPaginate = true
            this.initializeClientes = false
        },
        clickEvent: function (divModal, routeLoad, id, typeRequest) {
            return responseModal(divModal, routeLoad, id, typeRequest)
        }
    }
})
