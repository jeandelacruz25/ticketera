var objTableTareasTicket = {
    dataTareas: [],
    pagination: {
        current_page: 1
    },
    initializeTareas: true,
    initializeLoadPaginate: true,
    initDataPagination: false,
    searchTarea: '',
    selectUsuario: '',
    selectCliente: '',
    selectEstadoDev: '',
    selectEstado: [1, 2, 3],
    selectTicket: ''
}

var vmTableTareasTicket = new Vue({
    el: '#tableTareasTicketVue',
    data: objTableTareasTicket,
    computed: {
        nameCliente() {
            if (this.dataTareas.length > 0) {
                return this.dataTareas.map(function (item) {
                    return item.ticket.cliente.cliente
                })
            }
        },
        nameTarea() {
            if(this.dataTareas.length > 0){
                return this.dataTareas.map(function (item) {
                    let nameTarea = item.tarea
                    return nameTarea.length > 45 ? nameTarea.substring(0,45)+'..' : nameTarea
                })
            }
        },
        fechaInicio() {
            if (this.dataTareas.length > 0) {
                return this.dataTareas.map(function (item) {
                    let fechaInicio = item.fecha_inicio
                    return  moment(fechaInicio).isValid() ? moment(fechaInicio).format('DD/MM/YY HH:mm') : '-'
                })
            }
        },
        fechaCierre() {
            if (this.dataTareas.length > 0) {
                return this.dataTareas.map(function (item) {
                    let fechaCierre = item.fecha_cierre
                    return  moment(fechaCierre).isValid() ? moment(fechaCierre).format('DD/MM/YY HH:mm') : '-'
                })
            }
        },
        fechaVenc() {
            if (this.dataTareas.length > 0) {
                return this.dataTareas.map(function (item) {
                    let fechVenc = item.fecha_venc
                    return  moment(fechVenc).isValid() ? moment(fechVenc).format('DD/MM/YY HH:mm') : '-'
                })
            }
        },
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        utilsTable: function () {
            setTimeout(function() {
                $('.selectBoostrap').selectpicker({
                    style: "btn-default"
                })
                $('[data-tooltip="tooltip"]').tooltip()
            }, 100)
        },
        fetchTareas: async function () {
            this.initializeTareas = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationTareas`, {
                params: {
                    page: this.pagination.current_page,
                    searchTarea: this.searchTarea,
                    selectUsuario: this.selectUsuario,
                    selectCliente: this.selectCliente,
                    selectEstadoDev: this.selectEstadoDev,
                    selectEstado: this.selectEstado,
                }
            }, 'get')
            this.dataTareas = response.data.data
            this.pagination = response.pagination
            this.initializeTareas = false
            this.searchTarea = ''
            this.utilsTable()
        },
        fetchTareasTicket: async function (idTicket) {
            this.initializeTareas = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationTareasTicket`, {
                params: {
                    page: this.pagination.current_page,
                    searchTicket: idTicket
                }
            }, 'get')
            this.dataTareas = response.data.data
            this.pagination = response.pagination
            this.initializeTareas = false
            this.searchTarea = ''
            this.selectTicket = idTicket
            this.utilsTable()
        },
        reloadTareasTable: async function () {
            let ticketID = this.selectTicket
            if(ticketID.length > 0){
                await this.paginationFetchTareasTicket()
            }else{
                await this.paginationFetchTareas()
            }
        },
        paginationFetchTareas: async function () {
            if(this.dataTareas.length === 0){
                this.initializeTareas = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationTareas`, {
                params: {
                    page: this.pagination.current_page,
                    searchTarea: this.searchTarea,
                    selectUsuario: this.selectUsuario,
                    selectCliente: this.selectCliente,
                    selectEstadoDev: this.selectEstadoDev,
                    selectEstado: this.selectEstado,
                }
            }, 'get')
            this.dataTareas = response.data.data
            this.pagination = response.pagination

            if(this.dataTareas.length > 0){
                this.initDataPagination = true
            }else{
                this.initDataPagination = false
            }

            this.initializeLoadPaginate = true
            this.initializeTareas = false
            this.utilsTable()
        },
        paginationFetchTareasTicket: async function () {
            if(this.dataTareas.length === 0){
                this.initializeTareas = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationTareasTicket`, {
                params: {
                    page: this.pagination.current_page,
                    searchTicket: this.selectTicket
                }
            }, 'get')
            this.dataTareas = response.data.data
            this.pagination = response.pagination

            if(this.dataTareas.length > 0){
                this.initDataPagination = true
            }else{
                this.initDataPagination = false
            }

            this.initializeLoadPaginate = true
            this.initializeTareas = false
            this.utilsTable()
        },
        clickEvent: function (divModal, routeLoad, id, typeRequest) {
            return responseModal(divModal, routeLoad, id, typeRequest)
        }
    }
})
