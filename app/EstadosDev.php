<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosDev extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'estados_dev';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'estado', 'color',
    ];
}
