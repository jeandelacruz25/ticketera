<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection   = 'mysql';
    protected $table        = 'users';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'name', 'email', 'username', 'password', 'id_grupo', 'id_rol',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function grupo(){
        return $this->hasOne('App\Grupos', 'id','id_grupo');
    }
}
