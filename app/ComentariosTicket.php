<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentariosTicket extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'comentarios_ticket';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'id_ticket', 'comentario', 'fecha_reg', 'user_reg', 'id_estado',
    ];

    public function usuario(){
        return $this->hasOne('App\User', 'id','user_reg');
    }
}
