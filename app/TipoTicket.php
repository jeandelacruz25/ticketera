<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoTicket extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'tipo_ticket';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'tipoticket', 'peso',
    ];
}
