<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoOrigen extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'tipo_origen';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'tipo_origen',
    ];
}
