<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tareas extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'tareas';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $casts = [
        'orden' => 'double'
    ];

    protected $fillable = [
        'id', 'id_ticket', 'tarea', 'descripcion', 'id_usuario', 'fecha_venc', 'fecha_inicio', 'fecha_cierre', 'id_estadodev', 'id_estadotarea', 'fecha_reg', 'user_reg', 'origen', 'orden', 'dias_venc', 'id_tipodificultad', 'posicion',
    ];

    public function ticket(){
        return $this->hasOne('App\Tickets', 'id','id_ticket');
    }

    public function usuario(){
        return $this->hasOne('App\User', 'id','id_usuario');
    }

    public function env(){
        return $this->hasOne('App\EstadosDev', 'id','id_estadodev');
    }

    public function estado(){
        return $this->hasOne('App\EstadosTareas', 'id','id_estadotarea');
    }
}
