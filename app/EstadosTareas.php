<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosTareas extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'estados_tareas';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'estado', 'color',
    ];
}
