<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'tickets';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'id_tipoticket', 'id_cliente', 'asunto', 'detalle', 'solicitante', 'id_tipoorigen', 'id_tipoprioridad', 'id_tipodificultad', 'id_grupo', 'fecha_inicio', 'fecha_cierre', 'fecha_venc', 'nro_tareas', 'avance', 'id_estado', 'fecha_reg', 'user_reg', 'orden',
    ];

    public function cliente(){
        return $this->hasOne('App\Clientes', 'id','id_cliente');
    }

    public function tipo(){
        return $this->hasOne('App\TipoTicket', 'id','id_tipoticket');
    }

    public function origen(){
        return $this->hasOne('App\TipoOrigen', 'id','id_tipoorigen');
    }

    public function prioridad(){
        return $this->hasOne('App\TipoPrioridad', 'id','id_tipoprioridad');
    }

    public function dificultad(){
        return $this->hasOne('App\TipoDificultad', 'id','id_tipodificultad');
    }

    public function grupo(){
        return $this->hasOne('App\Grupos', 'id','id_grupo');
    }

    public function tareas(){
        return $this->hasMany('App\Tareas', 'id_ticket','id');
    }

    public function estado(){
        return $this->hasOne('App\EstadosTicket', 'id','id_estado');
    }
}
