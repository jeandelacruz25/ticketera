<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'clientes';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'cliente', 'persona_contacto', 'telefono_contacto', 'email_contacto', 'observacion', 'peso',
    ];
}
