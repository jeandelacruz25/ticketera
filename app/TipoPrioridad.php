<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPrioridad extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'tipo_prioridad';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'tipo_prioridad', 'prioridad',
    ];
}
