<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\EstadosDev;
use App\EstadosTareas;
use App\Http\Requests\formTareaRequest;
use App\Tareas;
use App\Tickets;
use App\TipoDificultad;
use App\TipoTicket;
use App\User;
use Carbon\Carbon;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class TareasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('elements/tareas/index')->with(array(
            'titleModule'       =>  'Tareas',
            'titleSubModule'    =>  'Listado',
            'titleBox'          =>  'Listado Tareas',
            'dataEstado'        =>  EstadosTareas::all(),
            'dataCliente'       =>  Clientes::all(),
            'dataUsuario'       =>  User::all(),
            'dataEstadoDev'     =>  EstadosDev::all(),
            'dataTipo'          =>  TipoTicket::all()
        ));
    }

    public function paginationTareas(Request $request){
        $whereCustom = array();

        if(Auth::user()->id_rol != 1){
            $where = ['id_usuario', '=', Auth::id()];
            $whereIn = [1, 2];
            //$whereIn2 = [1, 2];
            array_push($whereCustom, $where);
        }else{
            $whereIn = [1, 2, 3, 4];
            //$whereIn2 = [1, 2, 3, 4, 5];
        }

        if($request->searchTarea){
            $arraySearch = ['tarea', 'LIKE', '%'.$request->searchTarea.'%'];
            array_push($whereCustom, $arraySearch);
        }

        if($request->selectUsuario){
            $arraySearch = ['id_usuario', '=', $request->selectUsuario];
            array_push($whereCustom, $arraySearch);
        }

        if($request->searchTicket){
            $arraySearch = ['id_ticket', '=', $request->searchTicket];
            array_push($whereCustom, $arraySearch);
        }

        $tareas = Tareas::Select()
            ->with(['ticket', 'ticket.cliente', 'usuario', 'env', 'estado'])
            ->where($whereCustom)
            ->whereHas('ticket', function ($query) use($request) {
                if($request->selectCliente) {
                    $query->where('id_cliente', '=', $request->selectCliente);
                }
                if($request->selectTipo) {
                    $query->where('id_tipoticket', '=', $request->selectTipo);
                }
            })
            ->whereIn('id_estadotarea', $request->selectEstado ? $request->selectEstado : [1, 2, 3, 4, 5, 6])
            ->whereIn('id_estadodev', $whereIn)
            ->orderBy('posicion', 'asc')
            ->orderBy('orden', 'desc')
            ->paginate(30);

        $response = [
            'pagination' => [
                'total' => $tareas->total(),
                'per_page' => $tareas->perPage(),
                'current_page' => $tareas->currentPage(),
                'last_page' => $tareas->lastPage(),
                'from' => $tareas->firstItem(),
                'to' => $tareas->lastItem()
            ],
            'data' => $tareas
        ];

        return response()->json($response);
    }

    public function paginationTareasTicket(Request $request){
        $whereCustom = array();

        if($request->searchTicket){
            $arraySearch = ['id_ticket', '=', $request->searchTicket];
            array_push($whereCustom, $arraySearch);
        }

        $tareas = Tareas::Select()
            ->with(['ticket', 'ticket.cliente', 'ticket.tipo', 'usuario', 'env', 'estado'])
            ->where($whereCustom)
            ->orderBy('fecha_venc', 'asc')
            ->paginate(30);

        $response = [
            'pagination' => [
                'total' => $tareas->total(),
                'per_page' => $tareas->perPage(),
                'current_page' => $tareas->currentPage(),
                'last_page' => $tareas->lastPage(),
                'from' => $tareas->firstItem(),
                'to' => $tareas->lastItem()
            ],
            'data' => $tareas
        ];

        return response()->json($response);
    }

    public function downloadReportTareas(Request $request){
        $whereCustom = array();

        $whereIn = [1, 2, 3, 4];

        if($request->searchTarea){
            $arraySearch = ['tarea', 'LIKE', '%'.$request->searchTarea.'%'];
            array_push($whereCustom, $arraySearch);
        }

        if($request->selectUsuario){
            $arraySearch = ['id_usuario', '=', $request->selectUsuario];
            array_push($whereCustom, $arraySearch);
        }

        if($request->searchTicket){
            $arraySearch = ['id_ticket', '=', $request->searchTicket];
            array_push($whereCustom, $arraySearch);
        }

        $tareas = Tareas::Select()
            ->with(['ticket', 'ticket.cliente', 'ticket.tipo', 'usuario', 'env', 'estado'])
            ->where($whereCustom)
            ->whereHas('ticket', function ($query) use($request) {
                if($request->selectCliente) {
                    $query->where('id_cliente', '=', $request->selectCliente);
                }
                if($request->selectTipo) {
                    $query->where('id_tipoticket', '=', $request->selectTipo);
                }
            })
            ->whereIn('id_estadotarea', $request->selectEstado ? $request->selectEstado : [1, 2, 3, 4, 5, 6])
            ->whereIn('id_estadodev', $whereIn)
            ->orderBy('posicion', 'asc')
            ->orderBy('orden', 'desc')
            ->get()->toArray();

        if($tareas){
            $nameReport = 'reporte_tareas_'.time();
            Excel::create($nameReport, function($excel) use($tareas) {
                $excel->sheet('tareas', function($sheet) use($tareas) {
                    $sheet->fromArray($this->builderExportReport($tareas));
                });
            })->store('xlsx', 'exports');

            return [
                'success'   => true,
                'path'      => asset('exports/'.$nameReport.'.xlsx')
            ];
        }
        return response()->json([], 422);
    }

    protected function builderExportReport($tareas_list_query)
    {
        $posicion = 0;
        $idList = 0;
        foreach ($tareas_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['cliente']                  = $query['ticket']['cliente']['cliente'];
            $builderview[$posicion]['tipo']                     = $query['ticket']['tipo']['tipoticket'];
            $builderview[$posicion]['tarea']                    = $query['tarea'];
            $builderview[$posicion]['usuario']                  = $query['usuario']['name'];
            $builderview[$posicion]['fecha_registro']           = $query['ticket']['fecha_reg'] ? Carbon::parse($query['ticket']['fecha_reg'])->format('d/m/Y H:i a') : '-';
            $builderview[$posicion]['fecha_inicio']             = $query['fecha_inicio'] ? Carbon::parse($query['fecha_inicio'])->format('d/m/Y H:i a') : '-';
            $builderview[$posicion]['fecha_vencimiento']        = $query['fecha_venc'] ? Carbon::parse($query['fecha_venc'])->format('d/m/Y H:i a') : '-';
            $builderview[$posicion]['fecha_cierre']             = $query['fecha_cierre'] ? Carbon::parse($query['fecha_cierre'])->format('d/m/Y H:i a') : '-';
            $builderview[$posicion]['estado_dev']               = $query['env']['estado'];
            $builderview[$posicion]['estado_tarea']             = $query['estado']['estado'];
            $builderview[$posicion]['peso']                     = $query['ticket']['orden'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    public function formTareas(Request $request){
        $getUsuarios = User::all();
        $getEstadoDev = EstadosDev::all();
        $getEstadoTarea = EstadosTareas::all();
        $getDificultad = TipoDificultad::all();
        if(!empty($request->valueID['idTarea'])){
            $getDataTarea = $this->getTarea($request->valueID['idTarea']);
            $getDataTicket = $this->getTicket($request->valueID['idTicket']);
            return view('elements/tareas/form/form_tareas')->with(array(
                'dataTarea'             =>  $getDataTarea,
                'dataTicket'            =>  $getDataTicket,
                'dataEstadoDev'         =>  $getEstadoDev,
                'dataEstado'            =>  $getEstadoTarea,
                'idTicket'              =>  $request->valueID['idTicket'],
                'dataDificultad'        =>  $getDificultad,
                'dataUsuarios'          =>  $getUsuarios,
                'updateForm'            =>  true,
                'ticketTask'            =>  $request->valueID['ticketTask']
            ));
        }else{
            $getDataTicket = $this->getTicket($request->valueID['idTicket']);
            return view('elements/tareas/form/form_tareas')->with(array(
                'dataTarea'             =>  '',
                'dataTicket'            =>  $getDataTicket,
                'dataEstadoDev'         =>  '',
                'dataEstado'            =>  '',
                'idTicket'              =>  $request->valueID['idTicket'],
                'dataDificultad'        =>  $getDificultad,
                'dataUsuarios'          =>  $getUsuarios,
                'updateForm'            =>  false,
                'ticketTask'            =>  $request->valueID['ticketTask']
            ));
        }
    }

    public function saveFormTareas(formTareaRequest $request){
        if($request->taskID){
            try{
                DB::beginTransaction();

                Tareas::where('id', $request->taskID)
                    ->update([
                        'tarea'             =>  $request->nombreTarea,
                        'descripcion'       =>  $request->descripcionTarea,
                        'id_usuario'        =>  $request->selectUsuarios,
                        'fecha_venc'        =>  $request->fechaVenc && $request->horaVenc ? $request->fechaVenc.' '.$request->horaVenc.':00' : null,
                        'fecha_inicio'      =>  $request->fechaInicio && $request->horaInicio ? $request->fechaInicio.' '.$request->horaInicio.':00' : null,
                        'fecha_cierre'      =>  $request->fechaCierre && $request->horaCierre ? $request->fechaCierre.' '.$request->horaCierre.':00' : null,
                        'id_estadodev'      =>  $request->estadoDev,
                        'id_estadotarea'    =>  $request->estadoTicket,
                        'id_tipodificultad' =>  $request->dificultadTarea
                    ]);

                DB::statement("CALL SP_UPDATE_TAREA(".$request->ticketID.", ".$request->taskID.")");

                DB::commit();

                $serverNode = env('NODE_URL', 'http://tickets.securitec.pe:3888');
                $clientNode = new \ElephantIO\Client(new Version2X($serverNode, []));
                $clientNode->initialize();

                $clientNode->emit('reloadTableTareas', []);
                $clientNode->close();

                return ['message' => 'Success', 'action' => 'update', 'ticketTask' => $request->taskTicket];
            }catch (\Exception $e){
                DB::rollback();

                dd($e->getMessage());
                return ['message' => 'Error'];
            }
        }else{
            try{
                DB::beginTransaction();

                Tareas::create([
                    'id_ticket'         =>  $request->ticketID,
                    'tarea'             =>  $request->nombreTarea,
                    'descripcion'       =>  $request->descripcionTarea,
                    'id_usuario'        =>  $request->selectUsuarios,
                    'fecha_venc'        =>  $request->fechaVenc.' '.$request->horaVenc.':00',
                    'fecha_inicio'      =>  null,
                    'fecha_cierre'      =>  null,
                    'id_estadodev'      =>  $request->estadoDev,
                    'id_estadotarea'    =>  $request->estadoTicket,
                    'fecha_reg'         =>  Carbon::now(),
                    'user_reg'          =>  Auth::id(),
                    'origen'            =>  0,
                    'orden'             =>  0,
                    'dias_venc'         =>  0,
                    'id_tipodificultad' =>  $request->dificultadTarea,
                    'posicion'          =>  1
                ]);

                DB::commit();

                $idTask = Tareas::where('id_ticket', $request->ticketID)->orderBy('id', 'desc')->first();

                DB::statement("CALL SP_UPDATE_TAREA(".$request->ticketID.", ".$idTask['id'].")");

                $serverNode = env('NODE_URL', 'http://tickets.securitec.pe:3888');
                $clientNode = new \ElephantIO\Client(new Version2X($serverNode, []));
                $clientNode->initialize();

                $clientNode->emit('reloadTableTareas', []);
                $clientNode->close();

                return ['message' => 'Success', 'action' => 'create', 'ticketTask' => false];
            }catch (\Exception $e){
                DB::rollback();

                dd($e->getMessage());
                return ['message' => 'Error'];
            }
        }
    }

    public function formChangeStatusTask(Request $request){
        return view('elements/tareas/form/form_tareas_status')->with(array(
            'idTask'        =>  $request->valueID['idTask'],
            'idStatus'      =>  $request->valueID['idStatus']
        ));
    }

    public function saveFormChangeStatusTask(Request $request){
        try{
            $arrayCustom = [];

            if($request->statusID == 2){
                $arrayMerge = [
                    'id_estadodev'      => 3,
                    'fecha_cierre'      => Carbon::now(),
                    'id_estadotarea'    => 3,
                    'posicion'          => 1
                ];
                array_push($arrayCustom, $arrayMerge);
            }else{
                $arrayMerge = [
                    'id_estadodev'  => 2,
                    'fecha_inicio'  => Carbon::now()
                ];
                array_push($arrayCustom, $arrayMerge);
            }

            Tareas::where('id', $request->taskID)
                ->update($arrayMerge);

            return ['message' => 'Success'];
        }catch (\Exception $e){
            return ['message' => 'Error'];
        }
    }

    public function formViewTicket(Request $request){
        $getTicket = $this->getTicket($request->valueID['idTicket']);
        $getTask = $this->getTarea($request->valueID['idTarea']);
        return view('elements/tareas/form/form_tareas_ticket')->with(array(
            'dataTicket'    => $getTicket,
            'dataTarea'     => $getTask
        ));
    }

    protected function getAllTareas(){
        $whereCustom = array();

        if(Auth::user()->id_rol != 1){
            $where = ['id_usuario', '=', Auth::id()];
            $whereIn = [1, 2];
            $whereIn2 = [1, 2];
            array_push($whereCustom, $where);
        }else{
            $whereIn = [1, 2, 3, 4];
            $whereIn2 = [1, 2, 3, 4, 5];
        }

        $getDataTareas = Tareas::select()
            ->where($whereCustom)
            ->whereIn('id_estadodev', $whereIn)
            ->whereIn('id_estadotarea', $whereIn2)
            ->with(['ticket', 'ticket.cliente', 'usuario', 'env', 'estado'])
            ->orderBy('orden', 'desc')
            ->get()->toArray();

        return $getDataTareas;
    }

    protected function getTicket($idTarea){
        $dataTicket = Tickets::select()
            ->with(['cliente', 'tipo', 'origen', 'prioridad', 'dificultad', 'grupo', 'tareas', 'tareas.usuario', 'tareas.env', 'tareas.estado', 'estado'])
            ->where('id', $idTarea)
            ->get()->first();

        return $dataTicket;
    }

    protected function getTarea($idTarea){
        $getTarea = Tareas::where('id', $idTarea)
            ->with(['ticket', 'ticket.cliente', 'usuario', 'env', 'estado'])
            ->get()
            ->first();

        return $getTarea;
    }
}
