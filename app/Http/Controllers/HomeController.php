<?php

namespace App\Http\Controllers;

use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serverNode = env('NODE_URL', 'http://tickets.securitec.pe:3888');
        $clientNode = new \ElephantIO\Client(new Version2X($serverNode, []));
        $clientNode->initialize();

        $clientNode->emit('reloadTableTareas', []);
        $clientNode->close();
        return view('home');
    }
}
