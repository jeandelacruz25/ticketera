<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\ComentariosTicket;
use App\EstadosTicket;
use App\Grupos;
use App\Http\Requests\formChangeUsersRequest;
use App\Http\Requests\formTicketRequest;
use App\Tareas;
use App\Tickets;
use App\TipoDificultad;
use App\TipoOrigen;
use App\TipoPrioridad;
use App\TipoTicket;
use App\User;
use Carbon\Carbon;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravolt\Avatar\Facade as Avatar;
use Maatwebsite\Excel\Facades\Excel;

class TicketsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('elements/tickets/index')->with(array(
            'titleModule'       =>  'Tickets',
            'titleSubModule'    =>  'Listado',
            'titleBox'          =>  'Listado Tickets',
            'dataEstado'        =>  EstadosTicket::all(),
            'dataCliente'       =>  Clientes::all(),
            'dataTipo'          =>  TipoTicket::all()
        ));
    }

    public function formTickets(Request $request){
        $getCiente = Clientes::all();
        $getTipo = TipoTicket::all();
        $getOrigen = TipoOrigen::all();
        $getPrioridad = TipoPrioridad::all();
        $getDificultad = TipoDificultad::all();
        $getGrupo = Grupos::all();

        if($request->valueID){
            $getTicket = $this->getTicket($request->valueID);
            return view('elements/tickets/form/form_ticket')->with(array(
                'dataTicket'            => $getTicket,
                'dataCliente'           => $getCiente,
                'dataTipo'              => $getTipo,
                'dataOrigen'            => $getOrigen,
                'dataPrioridad'         => $getPrioridad,
                'dataDificultad'        => $getDificultad,
                'dataGrupo'             => $getGrupo,
                'updateForm'            => true
            ));
        }else{
            return view('elements/tickets/form/form_ticket')->with(array(
                'dataTicket'            => '',
                'dataCliente'           => $getCiente,
                'dataTipo'              => $getTipo,
                'dataOrigen'            => $getOrigen,
                'dataPrioridad'         => $getPrioridad,
                'dataDificultad'        => $getDificultad,
                'dataGrupo'             => $getGrupo,
                'updateForm'            => false
            ));
        }
    }

    public function paginationTickets(Request $request){
        $arrayWhere = [];

        if($request->searchTicket){
            $arraySearch = ['asunto', 'LIKE', '%'.$request->searchTicket.'%'];
            array_push($arrayWhere, $arraySearch);
        }

        if($request->selectCliente){
            $arraySearch = ['id_cliente', '=', $request->selectCliente];
            array_push($arrayWhere, $arraySearch);
        }

        if($request->selectTipo){
            $arraySearch = ['id_tipoticket', '=', $request->selectTipo];
            array_push($arrayWhere, $arraySearch);
        }

        $tickets = Tickets::Select('tickets.*')
            ->where($arrayWhere)
            ->whereIn('id_estado', $request->selectEstado ? $request->selectEstado : [1, 2, 3, 4, 5, 6])
            ->with(['cliente', 'tipo', 'origen', 'prioridad', 'dificultad', 'grupo', 'tareas', 'estado'])
            ->withCount('tareas')
            ->join('estados_ticket', 'tickets.id_estado', '=', 'estados_ticket.id')
            ->orderBy('tickets.id_tipoticket', 'asc')
            ->orderBy('estados_ticket.order', 'asc')
            ->orderBy('tickets.orden', 'desc')
            ->paginate(30);

        $response = [
            'pagination' => [
                'total' => $tickets->total(),
                'per_page' => $tickets->perPage(),
                'current_page' => $tickets->currentPage(),
                'last_page' => $tickets->lastPage(),
                'from' => $tickets->firstItem(),
                'to' => $tickets->lastItem()
            ],
            'data' => $tickets
        ];

        return response()->json($response);
    }

    public function saveFormTickets(formTicketRequest $request){
        try {
            DB::beginTransaction();

            $ticketQuery = Tickets::updateOrCreate([
                'id'                => $request->ticketID
            ], [
                'id_tipoticket'         => $request->tipoTicket,
                'id_cliente'            => $request->selectCliente,
                'asunto'                => $request->asuntoTicket ? $request->asuntoTicket : '-',
                'detalle'               => $request->detalleTicket ? $request->detalleTicket : '-',
                'solicitante'           => $request->solicitanteTicket ? $request->solicitanteTicket : '-',
                'id_tipoorigen'         => $request->origenTicket,
                'id_tipoprioridad'      => $request->prioridadTicket,
                'id_tipodificultad'     => $request->dificultadTicket,
                'id_grupo'              => $request->grupoTicket,
                'fecha_inicio'          => $request->fecInicio ? $request->fecInicio : null,
                'fecha_cierre'          => $request->fecCierre ? $request->fecCierre : null,
                'fecha_venc'            => $request->fecVenc ? $request->fecVenc : null,
                'nro_tareas'            => $request->nroTarea ? $request->nroTarea : 0,
                'avance'                => $request->avance ? $request->avance : 0,
                'id_estado'             => $request->idEstado ? $request->idEstado : 1,
                'fecha_reg'             => $request->fechaRegistro && $request->horaRegistro ? $request->fechaRegistro.' '.$request->horaRegistro.':00' : null,
                'user_reg'              => $request->userReg ? $request->userReg : Auth::id(),
                'orden'                 => $request->orden ? $request->orden : 1,
            ]);

            $action = $request->ticketID ? 'update' : 'create';
            if($ticketQuery){
                $getLastID = Tickets::select()->orderBy('id', 'desc')->first();
                $ticketID = $request->ticketID ? $request->ticketID : $getLastID['id'];
                try{
                    DB::statement("CALL SP_UPDATE_TICKET(".$ticketID.")");

                    DB::commit();

                    if($request->ticketID){
                        return back()->with('success','Se actualizaron los datos con exito.');
                    }

                    $serverNode = env('NODE_URL', 'http://tickets.securitec.pe:3888');
                    $clientNode = new \ElephantIO\Client(new Version2X($serverNode, []));
                    $clientNode->initialize();

                    $clientNode->emit('reloadTableTareas', []);
                    $clientNode->close();

                    return ['message' => 'Success', 'action' => $action, 'nameTicket' => $request->asuntoTicket];
                }catch (\Exception $e){
                    DB::rollback();

                    if($request->ticketID){
                        dd($e->getMessage());
                        return back()->with('error','Error al actualizar los datos.');
                    }

                    dd($e->getMessage());
                    return ['message' => 'Error'];
                }
            }

        }catch (\Exception $e){
            dd($e->getMessage());
            return ['message' => 'Error'];
        }
    }

    public function editTicket(Request $request){
        $getCiente = Clientes::all();
        $getTipo = TipoTicket::all();
        $getOrigen = TipoOrigen::all();
        $getPrioridad = TipoPrioridad::all();
        $getDificultad = TipoDificultad::all();
        $getGrupo = Grupos::all();
        $getEstado = EstadosTicket::all();
        return view('elements/tickets/ver_ticket')->with(array(
            'dataTicket'            =>  $this->getTicket($request->id),
            'dataCliente'           =>  $getCiente,
            'dataTipo'              =>  $getTipo,
            'dataOrigen'            =>  $getOrigen,
            'dataPrioridad'         =>  $getPrioridad,
            'dataDificultad'        =>  $getDificultad,
            'dataGrupo'             =>  $getGrupo,
            'dataEstado'            =>  $getEstado,
            'titleModule'           =>  'Tickets'
        ));
    }

    public function formChangeUsers(Request $request){
        $getUsuarios = User::all();
        return view('elements/tickets/form/form_ticket_assigned')->with(array(
            'dataTicket'        =>  $this->getTicket($request->valueID),
            'dataUsuarios'      =>  $getUsuarios,
            'checkUsuarios'     =>  $request->checkInput
        ));
    }

    public function saveFormChangeUser(formChangeUsersRequest $request){
        try{
            foreach ($request->taskID as $key => $value){
                Tareas::where([
                    ['id', '=', $value],
                    ['id_ticket', '=', $request->ticketID]
                ])->update([
                    'id_usuario'    => $request->selectUsuarios
                ]);
            }

            $serverNode = env('NODE_URL', 'http://tickets.securitec.pe:3888');
            $clientNode = new \ElephantIO\Client(new Version2X($serverNode, []));
            $clientNode->initialize();

            $clientNode->emit('reloadTableTareas', []);
            $clientNode->close();

            return ['message' => 'Success'];
        }catch (\Exception $e){
            return ['message' => 'Error'];
        }
    }

    public function downloadReportTicket(Request $request){
        $arrayWhere = [];

        if($request->searchTicket){
            $arraySearch = ['asunto', 'LIKE', '%'.$request->searchTicket.'%'];
            array_push($arrayWhere, $arraySearch);
        }

        if($request->selectCliente){
            $arraySearch = ['id_cliente', '=', $request->selectCliente];
            array_push($arrayWhere, $arraySearch);
        }

        if($request->selectTipo){
            $arraySearch = ['id_tipoticket', '=', $request->selectTipo];
            array_push($arrayWhere, $arraySearch);
        }

        $tickets = Tickets::Select('tickets.*')
            ->where($arrayWhere)
            ->whereIn('id_estado', $request->selectEstado ? $request->selectEstado : [1, 2, 3, 4, 5, 6])
            ->with(['cliente', 'tipo', 'origen', 'prioridad', 'dificultad', 'grupo', 'tareas', 'estado'])
            ->withCount('tareas')
            ->join('estados_ticket', 'tickets.id_estado', '=', 'estados_ticket.id')
            ->orderBy('tickets.id_tipoticket', 'asc')
            ->orderBy('estados_ticket.order', 'asc')
            ->orderBy('tickets.orden', 'desc')
            ->get()->toArray();

        if($tickets){
            $nameReport = 'reporte_tickets_'.time();
            Excel::create($nameReport, function($excel) use($tickets) {
                $excel->sheet('tickets', function($sheet) use($tickets) {
                    $sheet->fromArray($this->builderExportReport($tickets));
                });
            })->store('xlsx', 'exports');

            return [
                'success'   => true,
                'path'      => asset('exports/'.$nameReport.'.xlsx')
            ];
        }
        return response()->json([], 422);
    }

    protected function builderExportReport($tickets_list_query)
    {
        $posicion = 0;
        $idList = 0;
        foreach ($tickets_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['cliente']                  = $query['cliente']['cliente'];
            $builderview[$posicion]['asunto']                   = $query['asunto'];
            $builderview[$posicion]['tipo_ticket']              = $query['tipo']['tipoticket'];
            $builderview[$posicion]['fecha_vencimiento']        = $query['fecha_venc'] ? Carbon::parse($query['fecha_venc'])->format('d/m/Y H:i a') : '-';
            $builderview[$posicion]['peso']                     = $query['orden'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function getAllTickets(){

        $dataTickets = Tickets::select()
            ->with(['cliente', 'tipo', 'origen', 'prioridad', 'dificultad', 'grupo', 'tareas', 'estado'])
            ->orderBy('orden', 'desc')
            ->get()->toArray();

        return $dataTickets;
    }

    protected function getTicket($idTicket){
        $dataTicket = Tickets::where('id', $idTicket)
            ->with(['cliente', 'tipo', 'origen', 'prioridad', 'dificultad', 'grupo', 'tareas', 'tareas.usuario', 'tareas.env', 'tareas.estado', 'estado'])
            ->get()->first();

        return $dataTicket;
    }

    public function getTicketsComentarios(Request $request){
        $idTicket = isset($request['idTicket']) ? $request['idTicket'] : false;

        if($idTicket){
            $getComentarios = ComentariosTicket::with('usuario')
                ->where('id_ticket', $idTicket)
                ->orderBy('fecha_reg', 'desc')->get()->toArray();

            $arrayAvatar = [];
            foreach ($getComentarios as $key => $value){
                $avatarUsuario = $this->getAvatarUsuario($value['usuario']['name']);
                array_push($arrayAvatar, $avatarUsuario);
            }

            return [
                'listComentarios'   => $getComentarios,
                'listAvatar'        => $arrayAvatar
            ];
        }

        return [];
    }

    public function saveTicketsComentarios(Request $request){
        try{
            ComentariosTicket::insert([
                'id_ticket'     => $request->idTicket,
                'comentario'    => $request->comentario,
                'fecha_reg'     => Carbon::now(),
                'user_reg'      => Auth::id(),
                'id_estado'     => 1
            ]);

            $serverNode = env('NODE_URL', 'http://tickets.securitec.pe:3888');
            $clientNode = new \ElephantIO\Client(new Version2X($serverNode, []));
            $clientNode->initialize();

            $clientNode->emit('reloadComentarios', []);
            $clientNode->close();

            return ['message' => 'Success'];
        }catch (\Exception $e){
            return ['message' => 'Error'];
        }
    }

    public function getAvatarUsuario($nombre){
        return Avatar::create($nombre)->setDimension(128, 128)->toBase64();
    }
}
