<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\Http\Requests\formClienteRequest;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('elements/clientes/index')->with(array(
            'titleModule'       =>  'Clientes',
            'titleSubModule'    =>  'Listado',
            'titleBox'          =>  'Listado Clientes',
            'dataClientes'      =>  $this->getAllClientes()
        ));
    }

    public function paginationClientes(Request $request){
        $clientes = Clientes::Select()
            ->where([
                ['cliente', 'LIKE', '%'.$request->searchCliente.'%']
            ])
            ->paginate(15);

        $response = [
            'pagination' => [
                'total' => $clientes->total(),
                'per_page' => $clientes->perPage(),
                'current_page' => $clientes->currentPage(),
                'last_page' => $clientes->lastPage(),
                'from' => $clientes->firstItem(),
                'to' => $clientes->lastItem()
            ],
            'data' => $clientes
        ];

        return response()->json($response);
    }

    public function formClientes(Request $request){
        if($request->valueID){
            $getCliente = $this->getCliente($request->valueID);
            return view('elements/clientes/form/form_cliente')->with(array(
                'dataCliente'           => $getCliente,
                'updateForm'            => true
            ));
        }else{
            return view('elements/clientes/form/form_cliente')->with(array(
                'dataCliente'           => '',
                'updateForm'            => false
            ));
        }
    }

    public function saveformClientes(formClienteRequest $request){
        $clientesQuery = Clientes::updateOrCreate([
            'id'                => $request->clienteID
        ], [
            'cliente'           => $request->nombreCliente ? $request->nombreCliente : '-',
            'persona_contacto'  => $request->personaContacto ? $request->personaContacto : '-',
            'telefono_contacto' => $request->telefonoContacto ? $request->telefonoContacto : '-',
            'email_contacto'    => $request->emailContacto ? $request->emailContacto : '-',
            'observacion'       => $request->observacion ? $request->observacion : '-',
            'peso'              => $request->pesoCliente ? $request->pesoCliente : 0
        ]);

        $action = $request->clienteID ? 'update' : 'create';

        if($request->clienteID){
            DB::statement("CALL SP_UPDATE_CLIENTES(".$request->clienteID.")");
            $serverNode = env('NODE_URL', 'http://tickets.securitec.pe:3888');
            $clientNode = new \ElephantIO\Client(new Version2X($serverNode, []));
            $clientNode->initialize();

            $clientNode->emit('reloadTableTareas', []);
            $clientNode->close();
        }

        if($clientesQuery){
            return ['message' => 'Success', 'action' => $action];
        }
        return ['message' => 'Error'];
    }

    protected function getAllClientes(){
        $dataClientes = Clientes::select()
            ->get()->toArray();

        return $dataClientes;
    }

    protected function getCliente($idCliente){
        $dataCliente = Clientes::where('id', $idCliente)
            ->get()->first();

        return $dataCliente;
    }
}
