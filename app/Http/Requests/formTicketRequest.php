<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'selectCliente'     => 'required',
            'asuntoTicket'      => 'required',
            'tipoTicket'        => 'required',
            'origenTicket'      => 'required',
            'prioridadTicket'   => 'required',
            'dificultadTicket'  => 'required',
            'fechaRegistro'     => 'required',
            'horaRegistro'      => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [
            'selectCliente.required'    => 'Debes seleccionar un cliente',
            'asuntoTicket.required'     => 'Debes ingresar un asunto',
            'tipoTicket.required'       => 'Debes seleccionar un tipo',
            'origenTicket.required'     => 'Debes seleccionar un origen',
            'prioridadTicket.required'  => 'Debes seleccionar una prioridad',
            'dificultadTicket.required' => 'Debes seleccionar una dificultad',
            'fechaRegistro.required'    => 'Debes seleccionar una fecha de registro',
            'horaRegistro.required'     => 'Debes seleccionar una hora de registro',
        ];

        return $messages;
    }
}
