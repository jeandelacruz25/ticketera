<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formChangeUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'selectUsuarios'          => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [
            'selectUsuarios.required'    => 'Debes seleccionar por lo menos a un usuario',
        ];

        return $messages;
    }
}
