<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formTareaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombreTarea'           => 'required',
            'descripcionTarea'      => 'required',
            'selectUsuarios'        => 'required',
            'fechaVenc'             => 'required',
            'dificultadTarea'       => 'required',
        ];


        return $rules;
    }

    public function messages()
    {
        $messages = [
            'nombreTarea.required' => 'Debes ingresar un nombre para la tarea',
            'descripcionTarea.required' => 'Debes ingresar una descripcion para la tarea',
            'selectUsuarios.required' => 'Debes seleccionar un usuario para la tarea',
            'fechaVenc.required' => 'Debes ingresar una fecha de vencimiento para la tarea',
            'dificultadTarea.required' => 'Debes seleccionar una dificultad para estas tarea',
        ];

        return $messages;
    }
}
