<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombreCliente'          => 'required',
            'pesoCliente'            => 'required|numeric',
        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [
            'nombreCliente.required'    => 'Debes ingresar un nombre para el cliente',
            'pesoCliente.required'      => 'Debes ingresar un peso para el cliente',
            'pesoCliente.numeric'       => 'El peso del cliente debe ser numerico',
        ];

        return $messages;
    }
}
