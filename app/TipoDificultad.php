<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDificultad extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'tipo_dificultad';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'tipo_dificultad', 'dificultad',
    ];
}
