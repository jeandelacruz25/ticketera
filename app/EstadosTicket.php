<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosTicket extends Model
{
    protected $connection   = 'mysql';
    protected $table        = 'estados_ticket';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'estado', 'color', 'order',
    ];
}
