let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.babel([
    'node_modules/socket.io-client/dist/socket.io.js',
], 'public/js/nodeClient.js').version();

mix.babel([
    '.env.js',
    'resources/assets/securitec/nodejs/initScore.js',
    'resources/assets/securitec/nodejs/onSocket.js'
], 'public/js/nodeTicketera.js').version();
